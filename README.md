# Sistem Informasi Perpustakaan

Ini adalah sebuah website sistem informasi perpustakaan untuk universitas yang dibuat dengan framework Laravel. Project ini dibuat dengan maksud untuk mengorganisir sistem perpustakaan seperti manajemen koleksi buku, peminjaman buku, dan keanggotaan perpustakaan.

Project ini merupakan MVP pertama yang berfokus pada beberapa poin di bawah ini.

1. Mahasiswa/Tamu mampu melihat koleksi buku
2. Mahasiswa mampu meminjam buku setelah keanggotaannya disetujui admin
3. Admin mampu mengelola data koleksi buku
4. Admin mampu mengelola permintaan peminjaman buku

Adapun ERDnya sebagai berikut

![Entity Relationship Diagram](/ERD%20Design%20MVP1.png)

---

Oiya, untuk fakultas dan jurusan tuh saya ngambil dari ITB. Ini paling gampang buat didapetin lengkapnya sih. Kalau misalnya mau bikin akun mahasiswa bisa coba pilih aja fakultas FSRD jurusan seni rupa biar cepet wkwkwk.
