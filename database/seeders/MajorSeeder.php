<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class MajorSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('majors')->insert([
            [
                'name' => 'Meteorologi',
                'abbreviation' => 'ME',
                'faculty_id' => 1,
            ],
            [
                'name' => 'Oseanografi',
                'abbreviation' => 'OS',
                'faculty_id' => 1,
            ],
            [
                'name' => 'Teknik Geodesi dan Geomatika',
                'abbreviation' => 'GD',
                'faculty_id' => 1,
            ],
            [
                'name' => 'Teknik Geologi',
                'abbreviation' => 'GL',
                'faculty_id' => 1,
            ]
        ]);
        DB::table('majors')->insert([
            [
                'name' => 'Aktuaria',
                'abbreviation' => 'AK',
                'faculty_id' => 2,
            ],
            [
                'name' => 'Astronomi',
                'abbreviation' => 'AS',
                'faculty_id' => 2,
            ],
            [
                'name' => 'Fisika',
                'abbreviation' => 'FI',
                'faculty_id' => 2,
            ],
            [
                'name' => 'Kimia',
                'abbreviation' => 'KI',
                'faculty_id' => 2,
            ],
            [
                'name' => 'Matematika',
                'abbreviation' => 'MA',
                'faculty_id' => 2,
            ]
        ]);
        DB::table('majors')->insert([
            [
                'name' => 'Design Interior',
                'abbreviation' => 'DI',
                'faculty_id' => 3,
            ],
            [
                'name' => 'Desain Komunikasi Visual',
                'abbreviation' => 'DKV',
                'faculty_id' => 3,
            ],
            [
                'name' => 'Desain Produk',
                'abbreviation' => 'DP',
                'faculty_id' => 3,
            ],
            [
                'name' => 'Kriya',
                'abbreviation' => 'KR',
                'faculty_id' => 3,
            ],
            [
                'name' => 'Seni Rupa',
                'abbreviation' => 'SR',
                'faculty_id' => 3,
            ]
        ]);
        DB::table('majors')->insert([
            [
                'name' => 'Teknik Dirgantara',
                'abbreviation' => 'AE',
                'faculty_id' => 4,
            ],
            [
                'name' => 'Teknik Material',
                'abbreviation' => 'MT',
                'faculty_id' => 4,
            ],
            [
                'name' => 'Teknik Mesin',
                'abbreviation' => 'MS',
                'faculty_id' => 4,
            ]
        ]);
        DB::table('majors')->insert([
            [
                'name' => 'Teknik Geofisika',
                'abbreviation' => 'TG',
                'faculty_id' => 5,
            ],
            [
                'name' => 'Teknik Metalurgi',
                'abbreviation' => 'MG',
                'faculty_id' => 5,
            ],
            [
                'name' => 'Teknik Perminyakan',
                'abbreviation' => 'TM',
                'faculty_id' => 5,
            ],
            [
                'name' => 'Teknik Pertambangan',
                'abbreviation' => 'TA',
                'faculty_id' => 5,
            ]
        ]);
        DB::table('majors')->insert([
            [
                'name' => 'Rekayasa Infrastruktur Lingkungan',
                'abbreviation' => 'IL',
                'faculty_id' => 6,
            ],
            [
                'name' => 'Teknik dan Pengelolaan Sumber Daya Air',
                'abbreviation' => 'SA',
                'faculty_id' => 6,
            ],
            [
                'name' => 'Teknik Kelautan',
                'abbreviation' => 'KL',
                'faculty_id' => 6,
            ],
            [
                'name' => 'Teknik Lingkungan',
                'abbreviation' => 'TL',
                'faculty_id' => 6,
            ],
            [
                'name' => 'Teknik Sipil',
                'abbreviation' => 'SI',
                'faculty_id' => 6,
            ]
        ]);
        DB::table('majors')->insert([
            [
                'name' => 'Manajemen Rekayasa Industri',
                'abbreviation' => 'MR',
                'faculty_id' => 7,
            ],
            [
                'name' => 'Teknik Bioenergi dan Kemurgi',
                'abbreviation' => 'TB',
                'faculty_id' => 7,
            ],
            [
                'name' => 'Teknik Fisika',
                'abbreviation' => 'TF',
                'faculty_id' => 7,
            ],
            [
                'name' => 'Teknik Industri',
                'abbreviation' => 'TI',
                'faculty_id' => 7,
            ],
            [
                'name' => 'Teknik Kimia',
                'abbreviation' => 'TK',
                'faculty_id' => 7,
            ],
            [
                'name' => 'Teknik Pangan',
                'abbreviation' => 'PG',
                'faculty_id' => 7,
            ]
        ]);
        DB::table('majors')->insert([
            [
                'name' => 'Arsitektur',
                'abbreviation' => 'AR',
                'faculty_id' => 8,
            ],
            [
                'name' => 'Perencanaan Wilayah dan Kota',
                'abbreviation' => 'PWK',
                'faculty_id' => 8,
            ]
        ]);
        DB::table('majors')->insert([
            [
                'name' => 'Kewirausahaan',
                'abbreviation' => 'MK',
                'faculty_id' => 9,
            ],
            [
                'name' => 'Manajemen',
                'abbreviation' => 'MB',
                'faculty_id' => 9,
            ]
        ]);
        DB::table('majors')->insert([
            [
                'name' => 'Farmasi Klinik dan Komunitas',
                'abbreviation' => 'FKK',
                'faculty_id' => 10,
            ],
            [
                'name' => 'Sains dan Teknologi Farmasi',
                'abbreviation' => 'FA',
                'faculty_id' => 10,
            ]
        ]);
        DB::table('majors')->insert([
            [
                'name' => 'Biologi',
                'abbreviation' => 'BI',
                'faculty_id' => 11,
            ],
            [
                'name' => 'Mikrobiologi',
                'abbreviation' => 'BM',
                'faculty_id' => 11,
            ],
            [
                'name' => 'Rekayasa Hayati',
                'abbreviation' => 'BE',
                'faculty_id' => 11,
            ],
            [
                'name' => 'Rekayasa Pertanian',
                'abbreviation' => 'BA',
                'faculty_id' => 11,
            ],
            [
                'name' => 'Rekayasa Kehutanan',
                'abbreviation' => 'BW',
                'faculty_id' => 11,
            ],
            [
                'name' => 'Teknologi Pascapanen',
                'abbreviation' => 'BP',
                'faculty_id' => 11,
            ]
        ]);
        DB::table('majors')->insert([
            [
                'name' => 'Sistem dan Teknologi Informasi',
                'abbreviation' => 'II',
                'faculty_id' => 12,
            ],
            [
                'name' => 'Teknik Biomedis',
                'abbreviation' => 'EB',
                'faculty_id' => 12,
            ],
            [
                'name' => 'Teknik Elektro',
                'abbreviation' => 'EL',
                'faculty_id' => 12,
            ],
            [
                'name' => 'Informatika',
                'abbreviation' => 'IF',
                'faculty_id' => 12,
            ],
            [
                'name' => 'Teknik Telekomunikasi',
                'abbreviation' => 'ET',
                'faculty_id' => 12,
            ],
            [
                'name' => 'Teknik Tenaga Listrik',
                'abbreviation' => 'EP',
                'faculty_id' => 12,
            ]
        ]);
    }
}
