<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class FacultySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('faculties')->insert([
            [
                'name' => 'Fakultas Ilmu dan Teknologi Kebumian',
                'abbreviation' => 'FITB',
            ],
            [
                'name' => 'Fakultas Matematika dan Ilmu Pengetahuan Alam',
                'abbreviation' => 'FMIPA',
            ],
            [
                'name' => 'Fakultas Seni Rupa dan Desain',
                'abbreviation' => 'FSRD',
            ],
            [
                'name' => 'Fakultas Teknik Mesin dan Dirgantara',
                'abbreviation' => 'FTMD',
            ],
            [
                'name' => 'Fakultas Teknik Pertambangan dan Perminyakan',
                'abbreviation' => 'FTTM',
            ],
            [
                'name' => 'Fakultas Teknik Sipil dan Lingkungan',
                'abbreviation' => 'FTSL',
            ],
            [
                'name' => 'Fakultas Teknologi Industri',
                'abbreviation' => 'FTI',
            ],
            [
                'name' => 'Sekolah Arsitektur, Perencanaan, dan Pengembangan Kebijakan',
                'abbreviation' => 'SAPPK',
            ],
            [
                'name' => 'Sekolah Bisnis dan Manajemen',
                'abbreviation' => 'SBM',
            ],
            [
                'name' => 'Sekolah Farmasi',
                'abbreviation' => 'SF',
            ],
            [
                'name' => 'Sekolah Ilmu dan Teknologi Hayati',
                'abbreviation' => 'SITH',
            ],
            [
                'name' => 'Sekolah Teknik Elektro dan Informatika',
                'abbreviation' => 'STEI',
            ]
        ]);
    }
}
