<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDetailsOfBooksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('details_of_books', function (Blueprint $table) {
            $table->unsignedBigInteger('book_id');
            $table->string('format')->nullable();
            $table->string('pagination')->nullable();
            $table->integer('number_of_pages')->nullable();
            $table->string('dimensions')->nullable();
            $table->string('language')->nullable();
            $table->text('synopsis')->nullable();
            $table->text('work_description')->nullable();
            $table->primary('book_id');
            $table->foreign('book_id')->references('id')->on('books')->onDelete('cascade');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('details_of_books');
    }
}
