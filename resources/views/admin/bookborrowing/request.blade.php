@extends('layouts.admin')

@section('pageTitle')
    Ajuan Peminjaman | Sistem Informasi Perpustakaan
@endsection

@section('bodySection')
    <div class="">
        <div class="fs-5 fw-bold text-bg-success rounded-3 px-3 py-2 bg-gradient">Ajuan Peminjaman</div>
        <hr>

        @if ($bookBorrowingRequests->count() != 0)
            <div class="text-muted">Menampilkan
                {{ $bookBorrowingRequests->perPage() > $bookBorrowingRequests->total() ? $bookBorrowingRequests->total() : $bookBorrowingRequests->perPage() }}
                dari
                {{ $bookBorrowingRequests->total() }} permintaan</div>
            <form action="{{ route('admin.bookborrowing.process') }}" method="post">
                @csrf
                @foreach ($bookBorrowingRequests as $bookBorrowingRequest)
                    <div class="card mb-3">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-3 col-md-2 col-xl-1">
                                    @if ($bookBorrowingRequest->book->cover_image != null)
                                        <img src="{{ asset('storage/' . $bookBorrowingRequest->book->cover_image) }}"
                                            class="img-fluid" style="max-height: 100px" alt="">
                                    @else
                                        <img src="{{ asset('img/image-not-found.png') }}" class="img-fluid"
                                            style="max-height: 100px" alt="">
                                    @endif
                                </div>
                                <div class="col-9 col-md-6 col-xl-8">
                                    <div class="fw-semibold">{{ $bookBorrowingRequest->book->title }}</div>
                                    <div class="d-none d-md-block">
                                        <div class="text-muted">By {{ $bookBorrowingRequest->book->author }},
                                            Publisher :
                                            {{ $bookBorrowingRequest->book->publisher }}</div>
                                    </div>
                                    <div class="">Permintaan dari {{ $bookBorrowingRequest->user->name }}
                                        id({{ $bookBorrowingRequest->user_id }})</div>
                                </div>
                                <div class="col-12 col-md-4 col-xl-3">
                                    <hr class="d-md-none">
                                    <div class="">
                                        Terima Permintaan?
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio"
                                            name="requestId[{{ $bookBorrowingRequest->id }}]"
                                            id="approve{{ $bookBorrowingRequest->id }}" value="1">
                                        <label class="form-check-label" for="approve{{ $bookBorrowingRequest->id }}">
                                            Ya, terima permintaan
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio"
                                            name="requestId[{{ $bookBorrowingRequest->id }}]"
                                            id="approve{{ $bookBorrowingRequest->id }}" value="0">
                                        <label class="form-check-label" for="approve{{ $bookBorrowingRequest->id }}">
                                            Tidak, tolak permintaan
                                        </label>
                                    </div>
                                    <div class="">
                                        {{ $bookBorrowingRequest->book->amount_available > 0 ? null : 'Stok Buku habis' }}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
                <button type="submit" class="btn btn-success bg-gradient w-100">Update Permintaan</button>
            </form>
            {{ $bookBorrowingRequests->links() }}
        @else
            <div class="fw-semibold">Tidak ada ajuan peminjaman pada saat ini</div>
        @endif
    </div>
@endsection
