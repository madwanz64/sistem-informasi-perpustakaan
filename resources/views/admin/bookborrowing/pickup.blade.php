@extends('layouts.admin')

@section('pageTitle')
    Ajuan Peminjaman | Sistem Informasi Perpustakaan
@endsection

@section('bodySection')
    <div class="">
        <div class="fs-5 fw-bold text-bg-success rounded-3 px-3 py-2 bg-gradient">Pengambilan Buku</div>
        <hr>

        <form action="{{ route('admin.bookborrowing.pickup') }}" method="get">
            <div class="input-group mb-3">
                <input type="number" name="user_id" class="form-control" placeholder="Masukkan User ID">
                <button class="btn btn-outline-success" type="submit"><i class="fas fa-search"></i></button>
            </div>
        </form>

        @if ($bookBorrowingRequests->count() != 0)
            <div class="text-muted">Menampilkan
                {{ $bookBorrowingRequests->perPage() > $bookBorrowingRequests->total() ? $bookBorrowingRequests->total() : $bookBorrowingRequests->perPage() }}
                dari
                {{ $bookBorrowingRequests->total() }} permintaan</div>
            @foreach ($bookBorrowingRequests as $bookBorrowingRequest)
                <div class="card mb-3" id="request{{ $bookBorrowingRequest->id }}">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-3 col-md-2 col-xl-1">
                                @if ($bookBorrowingRequest->book->cover_image != null)
                                    <img src="{{ asset('storage/' . $bookBorrowingRequest->book->cover_image) }}"
                                        class="img-fluid" style="max-height: 100px" alt="">
                                @else
                                    <img src="{{ asset('img/image-not-found.png') }}" class="img-fluid"
                                        style="max-height: 100px" alt="">
                                @endif
                            </div>
                            <div class="col-9 col-md-6 col-xl-8">
                                <div class="fw-semibold">{{ $bookBorrowingRequest->book->title }}</div>
                                <div class="d-none d-md-block">
                                    <div class="text-muted">By {{ $bookBorrowingRequest->book->author }},
                                        Publisher :
                                        {{ $bookBorrowingRequest->book->publisher }}</div>
                                </div>
                                <div class="">Permintaan dari {{ $bookBorrowingRequest->user->name }}
                                    id({{ $bookBorrowingRequest->user_id }})</div>
                                <div class="">Batas akhir pengambilan
                                    {{ \Carbon\Carbon::parse($bookBorrowingRequest->updated_at)->addDays(3)->format('d M Y') }}
                                </div>
                            </div>
                            <div class="col-12 col-md-4 col-xl-3">
                                <hr class="d-md-none">
                                <button type="button" class="btn btn-sm btn-success bg-gradient w-100 mb-3"
                                    onclick="bookActivated({{ $bookBorrowingRequest->id }})">Buku
                                    sudah diambil</button>
                                <button type="button" class="btn btn-sm btn-outline-danger bg-gradient w-100"
                                    onclick="bookCancelled({{ $bookBorrowingRequest->id }})">Buku
                                    batal diambil</button>

                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
            {{ $bookBorrowingRequests->links() }}
        @else
            <div class="fw-semibold">Tidak ada data buku yang akan diambil pada saat ini</div>
        @endif
    </div>
@endsection

@push('script')
    <script>
        function bookActivated(id) {
            console.log(id);
            let url = "{{ url('/') }}/admin/bookborrowing/" + id + "/active";
            let xhr = new XMLHttpRequest();

            xhr.open("POST", url, true);
            xhr.setRequestHeader("X-CSRF-TOKEN", "{{ csrf_token() }}");
            xhr.setRequestHeader("content-type", "application/json");
            xhr.onload = function() {
                var response = JSON.parse(xhr.response);
                if (xhr.readyState == 4 && xhr.status == "200") {
                    if (response['status'] == 200) {
                        let card = document.getElementById('request' + id);
                        card.parentNode.removeChild(card);
                        Toast.fire(
                            'Berhasil',
                            'Pengambilan buku berhasil direkap',
                            'success'
                        )
                    } else {
                        Toast.fire(
                            'Gagal',
                            'Pengambilan buku gagal direkap',
                            'error'
                        )
                    }
                } else {
                    Toast.fire(
                        'Gagal',
                        'Pengambilan buku gagal direkap',
                        'error'
                    )
                }
            }
            xhr.send();
        }

        function bookCancelled(id) {
            console.log(id);
            let url = "{{ url('/') }}/admin/bookborrowing/" + id + "/cancel";
            let xhr = new XMLHttpRequest();

            xhr.open("POST", url, true);
            xhr.setRequestHeader("X-CSRF-TOKEN", "{{ csrf_token() }}");
            xhr.setRequestHeader("content-type", "application/json");
            xhr.onload = function() {
                var response = JSON.parse(xhr.response);
                if (xhr.readyState == 4 && xhr.status == "200") {
                    if (response['status'] == 200) {
                        let card = document.getElementById('request' + id);
                        card.parentNode.removeChild(card);
                        Toast.fire(
                            'Berhasil',
                            'Pembatalan pengambilan buku berhasil direkap',
                            'success'
                        )
                    } else {
                        Toast.fire(
                            'Gagal',
                            'Pembatalan pengambilan buku gagal direkap',
                            'error'
                        )
                    }
                } else {
                    Toast.fire(
                        'Gagal',
                        'Pembatalan pengambilan buku gagal direkap',
                        'error'
                    )
                }
            }
            xhr.send();
        }
    </script>
@endpush
