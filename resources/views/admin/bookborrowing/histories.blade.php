@extends('layouts.admin')

@section('pageTitle')
    Ajuan Peminjaman | Sistem Informasi Perpustakaan
@endsection

@section('bodySection')
    <div class="">
        <div class="fs-5 fw-bold text-bg-success rounded-3 px-3 py-2 bg-gradient">Pengambilan Buku</div>
        <hr>
        @if ($bookBorrowingHistories->count() != 0)
            @foreach ($bookBorrowingHistories as $bookHistory)
                <div class="row mb-3">
                    <div class="col-3 col-md-2 col-xl-1">
                        @if ($bookHistory->book->cover_image != null)
                            <img src="{{ asset('storage/' . $bookHistory->book->cover_image) }}" class="img-fluid"
                                style="max-height: 100px" alt="">
                        @else
                            <img src="{{ asset('img/image-not-found.png') }}" class="img-fluid" style="max-height: 100px"
                                alt="">
                        @endif
                    </div>
                    <div class="col-9 col-md-10 col-xl-11">
                        <div class="fw-semibold">{{ $bookHistory->book->title }}</div>
                        <div class="d-none d-md-block">
                            <div class="text-muted">By {{ $bookHistory->book->author }}, Publisher :
                                {{ $bookHistory->book->publisher }}</div>
                        </div>
                        <div class="">Status : <b>{{ Str::upper($bookHistory->status) }}</b></div>
                        <div class="text-muted">
                            @if ($bookHistory->status == 'pending')
                                Diajukan tanggal
                            @elseif ($bookHistory->status == 'approved')
                                Disetujui tanggal
                            @elseif ($bookHistory->status == 'disapproved')
                                Ditolak tanggal
                            @elseif ($bookHistory->status == 'active')
                                Dipinjam sejak tanggal
                            @elseif ($bookHistory->status == 'returned')
                                Dikembalikan tanggal
                            @endif
                            {{ \Carbon\Carbon::parse(auth()->user()->updated_at)->format('d M Y') }}
                        </div>
                    </div>
                </div>
                <hr>
            @endforeach
            {{ $bookBorrowingHistories->links() }}
        @else
            <div class=" fw-semibold text-center">Tidak ada permintaan buku yang sedang diproses, diterima, ataupun buku
                yang sedang dipinjam</div>
        @endif
    </div>
@endsection
