@extends('layouts.admin')

@section('pageTitle')
    Dashboard Admin | Sistem Informasi Perpustakaan
@endsection

@section('bodySection')
    <div class="">
        <div class="fs-5 fw-bold text-bg-success rounded-3 px-3 py-2 bg-gradient">Dashboard</div>
        <hr>
        <div class="row">
            <div class="col-md-6">
                <div class=" fs-5 fw-semibold text-center text-uppercase mb-3">Buku</div>
                <div class="card mb-3">
                    <div class="card-header text-bg-success bg-gradient">
                        Total Eksemplar
                    </div>
                    <div class="card-body">
                        <div class="fs-3">{{ $amountBookTotal }} Eksemplar</div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xl-6">
                        <div class="card mb-3">
                            <div class="card-header text-bg-success bg-gradient">
                                Eksemplar Tersedia
                            </div>
                            <div class="card-body">
                                <div class="fs-3">{{ $amountBookAvailable }} Eksemplar</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-6">
                        <div class="card mb-3">
                            <div class="card-header text-bg-success bg-gradient">
                                Sedang Dipinjam
                            </div>
                            <div class="card-body">
                                <div class="fs-3">{{ $amountBookBorrowed }} Eksemplar</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class=" fs-5 fw-semibold text-center text-uppercase mb-3">Member</div>
                <div class="card mb-3">
                    <div class="card-header text-bg-success bg-gradient">
                        Banyak User
                    </div>
                    <div class="card-body">
                        <div class="fs-3">{{ $amountUser }} User</div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xl-6">
                        <div class="card mb-3">
                            <div class="card-header text-bg-success bg-gradient">
                                User yang Member
                            </div>
                            <div class="card-body">
                                <div class="fs-3">{{ $amountMember }} User</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-6">
                        <div class="card mb-3">
                            <div class="card-header text-bg-success bg-gradient">
                                User yang non-Member
                            </div>
                            <div class="card-body">
                                <div class="fs-3">{{ $amountNonMember }} User</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <div class=" fs-5 fw-semibold text-center text-uppercase mb-3">Peminjaman</div>
        <div class="row">
            <div class="col-md-6">
                <div class="card mb-3">
                    <div class="card-header text-bg-success bg-gradient">
                        Ajuan Peminjaman
                    </div>
                    <div class="card-body">
                        <div class="fs-3">{{ $amountPendingRequest }} Ajuan</div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="card mb-3">
                    <div class="card-header text-bg-success bg-gradient">
                        Total Peminjaman
                    </div>
                    <div class="card-body">
                        <div class="fs-3">{{ $amountTotalBorrowRequest }} Kali</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
