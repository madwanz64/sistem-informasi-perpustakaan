@extends('layouts.admin')

@section('pageTitle')
    Tambah Buku Baru | Sistem Informasi Perpustakaan
@endsection

@section('bodySection')
    <div class="">
        <div class="fs-5 fw-bold text-bg-success rounded-3 px-3 py-2 bg-gradient">Tambah Buku Baru</div>
        <hr>
        <form action="{{ route('admin.book.store') }}" method="post" enctype="multipart/form-data">
            @csrf
            <div class="mb-3">
                <label for="title" class="form-label">Judul Buku <span class="text-danger fw-bold">*</span></label>
                <input type="text" class="form-control @error('title') is-invalid @enderror" name="title" id="title"
                    value="{{ old('title') }}" placeholder="Judul Buku" required>
            </div>
            <div class="mb-3">
                <label for="author" class="form-label">Penulis Buku <span class="text-danger fw-bold">*</span></label>
                <input type="text" class="form-control @error('author') is-invalid @enderror" name="author" id="author"
                    aria-describedby="authorHelp" value="{{ old('author') }}" placeholder="Penulis Buku" required>
                <small id="authorHelp" class="form-text text-muted">Jika terdapat lebih dari satu penulis, pisahkan dengan
                    <span class="fw-bold">;</span></small>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="mb-3">
                        <label for="publisher" class="form-label">Penerbit <span
                                class="text-danger fw-bold">*</span></label>
                        <input type="text" class="form-control @error('publisher') is-invalid @enderror" name="publisher"
                            id="publisher" value="{{ old('publisher') }}" placeholder="Penerbit" required>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="mb-3">
                        <label for="published_in_city" class="form-label">Kota Terbit <span
                                class="text-danger fw-bold">*</span></label>
                        <input type="text" class="form-control @error('published_in_city') is-invalid @enderror"
                            name="published_in_city" id="published_in_city" value="{{ old('published_in_city') }}"
                            placeholder="Kota Terbit" required>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="mb-3">
                        <label for="published_in_year" class="form-label">Tahun Terbit <span
                                class="text-danger fw-bold">*</span></label>
                        <input type="number" class="form-control @error('published_in_year') is-invalid @enderror"
                            name="published_in_year" id="published_in_year" value="{{ old('published_in_year') }}"
                            placeholder="Tahun Terbit" required>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="mb-3">
                        <label for="location" class="form-label">Lokasi Buku <span
                                class="text-danger fw-bold">*</span></label>
                        <input type="text" class="form-control @error('location') is-invalid @enderror" name="location"
                            id="location" value="{{ old('location') }}" placeholder="Lokasi Buku" required>
                    </div>
                </div>
            </div>
            <div class="mb-3">
                <label for="cover_image" class="form-label">Gambar Cover</label>
                <input type="file" class="form-control @error('cover_image') is-invalid @enderror" name="cover_image"
                    id="cover_image" aria-describedby="coverImageHelp" placeholder="Gambar Cover">
                <small id="coverImageHelp" class="form-text text-muted">Pastikan gambar dapat terlihat dengan jelas</small>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="mb-3">
                        <label for="amount_total" class="form-label">Stok Buku <span
                                class="text-danger fw-bold">*</span></label>
                        <input type="number" class="form-control @error('amount_total') is-invalid @enderror"
                            name="amount_total" id="amount_total" value="{{ old('amount_total') }}"
                            placeholder="Stok Buku" required>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="mb-3">
                        <label for="borrowable" class="form-label">Apa Buku Dapat Dipinjam? <span
                                class="text-danger fw-bold">*</span></label>
                        <select class="form-control @error('borrowable') is-invalid @enderror" name="borrowable"
                            id="borrowable" required>
                            <option value="1" {{ old('borrowable') === 1 ? 'selected' : null }}>Ya, Buku Dapat Dipinjam
                            </option>
                            <option value="0" {{ old('borrowable') === 0 ? 'selected' : null }}>Tidak, Hanya Baca</option>
                        </select>
                    </div>
                </div>
            </div>


            <div class="accordion mb-3" id="informasiTambahan">
                <div class="accordion-item">
                    <h2 class="accordion-header" id="detailBukuTambahan">
                        <button class="accordion-button text-bg-success bg-gradient" type="button" data-bs-toggle="collapse"
                            data-bs-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                            Detail Buku Tambahan
                        </button>
                    </h2>
                    <div id="collapseOne" class="accordion-collapse collapse show" aria-labelledby="detailBukuTambahan"
                        data-bs-parent="#informasiTambahan">
                        <div class="accordion-body">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="mb-3">
                                        <label for="format" class="form-label">Format Buku</label>
                                        <input type="text" class="form-control @error('format') is-invalid @enderror"
                                            name="format" id="format" value="{{ old('format') }}"
                                            placeholder="Format Buku">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="mb-3">
                                        <label for="pagination" class="form-label">Pemberian nomor pada halaman
                                            buku</label>
                                        <input type="text" class="form-control @error('pagination') is-invalid @enderror"
                                            name="pagination" id="pagination" value="{{ old('pagination') }}"
                                            placeholder="Contoh : x, 1000 hlm.">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="mb-3">
                                        <label for="number_of_pages" class="form-label">Jumlah Halaman</label>
                                        <input type="number"
                                            class="form-control @error('number_of_pages') is-invalid @enderror"
                                            name="number_of_pages" id="number_of_pages"
                                            value="{{ old('number_of_pages') }}" placeholder="Contoh : 1000">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="mb-3">
                                        <label for="dimensions" class="form-label">Dimensi Buku</label>
                                        <input type="text" class="form-control @error('dimensions') is-invalid @enderror"
                                            name="dimensions" id="dimensions" value="{{ old('dimensions') }}"
                                            placeholder="Contoh : 17.5 x 25 cm">
                                    </div>
                                </div>
                            </div>
                            <div class="mb-3">
                                <label for="language" class="form-label">Bahasa</label>
                                <input type="text" class="form-control @error('language') is-invalid @enderror"
                                    name="language" id="language" value="{{ old('language') }}"
                                    placeholder="Contoh : Bahasa Indonesia">
                            </div>
                            <div class="mb-3">
                                <label for="synopsis" class="form-label">Sinopsis Buku</label>
                                <textarea class="form-control @error('synopsis') is-invalid @enderror" name="synopsis" id="synopsis" rows="3">{{ old('synopsis') }}</textarea>
                                <small class="text-muted">Ceritakan dengan singkat tentang isi buku</small>
                            </div>
                            <div class="mb-3">
                                <label for="work_description" class="form-label">Deskripsi Karya</label>
                                <textarea class="form-control @error('work_description') is-invalid @enderror" name="work_description"
                                    id="work_description" rows="3">{{ old('work_description') }}</textarea>
                                <small class="text-muted">Ceritakan dengan singkat tentang karya ini</small>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <button type="submit" class="btn btn-success bg-gradient w-100">Tambah Buku</button>
        </form>
    </div>
@endsection
