@extends('layouts.admin')

@section('pageTitle')
    Data Member | Sistem Informasi Perpustakaan
@endsection

@section('bodySection')
    <div class="">
        <div class="fs-5 fw-bold text-bg-success rounded-3 px-3 py-2 bg-gradient">Data Member</div>
        <hr>
        @forelse ($users as $user)
            <div class="card mb-3" id="user{{ $user->id }}">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-9">
                            <div class="fw-semibold">{{ $user->name }}</div>
                            <div class="text-muted">Status : {{ $user->is_member ? 'Member' : 'Bukan Member' }}</div>
                            <div class="">Bergabung sejak :
                                {{ \Carbon\Carbon::parse($user->created_at)->format('d M Y') }}</div>
                        </div>
                        <div class="col-md-3">
                            <hr class="d-md-none">
                            <a href="{{ route('admin.member.edit', ['member' => $user->id]) }}"
                                class="btn btn-sm btn-success bg-gradient w-100 mb-2">Edit Member</a>
                            <button type="button" class="btn btn-sm btn-outline-danger bg-gradient w-100"
                                onclick="removeUser({{ $user->id }})">Hapus
                                Member</button>
                        </div>
                    </div>
                </div>
            </div>
        @empty
            <div class="">Belum ada satupun member</div>
        @endforelse
        {{ $users->links() }}
    </div>
@endsection

@push('script')
    <script>
        function removeUser(id) {
            Swal.fire({
                title: 'Anda Yakin?',
                text: "User akan menjadi non-aktif dan tidak dapat login",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#198754',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Ya, hapus user!'
            }).then((result) => {
                if (result.isConfirmed) {

                    let userId = id;
                    let url = "/admin/member/";
                    let xhr = new XMLHttpRequest();

                    xhr.open("DELETE", url + userId, true);
                    xhr.setRequestHeader("X-CSRF-TOKEN", "{{ csrf_token() }}");
                    xhr.setRequestHeader("content-type", "application/json");
                    xhr.onload = function() {
                        var response = JSON.parse(xhr.response);
                        if (xhr.readyState == 4 && xhr.status == "200") {
                            if (response['status'] == 200) {
                                let card = document.getElementById('user' + id)
                                card.parentNode.removeChild(card)
                                Toast.fire(
                                    'Berhasil',
                                    'User berhasil dihapus',
                                    'success'
                                )
                            } else {
                                Toast.fire(
                                    'Gagal',
                                    'User gagal dihapus',
                                    'error'
                                )
                            }
                        } else {
                            Toast.fire(
                                'Gagal',
                                'User gagal dihapus',
                                'error'
                            )
                        }
                    }
                    xhr.send();

                }
            })
        }
    </script>
@endpush
