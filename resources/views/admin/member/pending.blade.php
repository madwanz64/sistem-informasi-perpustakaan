@extends('layouts.admin')

@section('pageTitle')
    Pendaftar Baru | Sistem Informasi Perpustakaan
@endsection

@section('bodySection')
    <div class="">
        <div class="fs-5 fw-bold text-bg-success rounded-3 px-3 py-2 bg-gradient">Pendaftar Baru</div>
        <hr>
        @forelse ($pendingUsers as $pendingUser)
            <div class="" id="pendingUser{{ $pendingUser->id }}">
                <div class="card mb-3">
                    <div class="card-body">
                        <div class="row d-flex align-items-center">
                            <div class="col-md-9">
                                <div class="fw-semibold">{{ $pendingUser->name }}</div>
                                <div class="text-muted">No. Telpon : {{ $pendingUser->profile->phone_number }}
                                </div>
                                <div class="">{{ $pendingUser->profile->address }}</div>
                            </div>
                            <div class="col-md-3">
                                <hr class="d-block d-md-none">
                                <button type="button" class="btn btn-success btn-sm bg-gradient w-100 mb-3"
                                    onclick="verifyMember({{ $pendingUser->id }})">Jadikan Member</button>
                                <button type="button" class="btn btn-outline-danger btn-sm bg-gradient w-100"
                                    onclick="rejectMember({{ $pendingUser->id }})">Data Belum Valid</button>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @empty
            <div class="fw-semibold">Belum ada lagi member baru yang daftar</div>
        @endforelse
    </div>
@endsection

@push('script')
    <script>
        function verifyMember(id) {
            console.log(id);
            var url = "/admin/member/verify/";
            var xhr = new XMLHttpRequest();

            xhr.open("PUT", url + id, true);
            xhr.setRequestHeader("X-CSRF-TOKEN", "{{ csrf_token() }}");
            xhr.setRequestHeader("content-type", "application/json");
            xhr.onload = function() {
                var response = JSON.parse(xhr.response);
                if (xhr.readyState == 4 && xhr.status == "200") {
                    if (response['status'] == 200) {
                        var element = document.getElementById('pendingUser' + id + '');
                        element.parentNode.removeChild(element);
                        Toast.fire(
                            'Berhasil',
                            'Member dengan id ' + id + ' berhasil diverifikasi',
                            'success'
                        )
                    } else {
                        Toast.fire(
                            'Gagal',
                            'Member dengan id ' + id + ' gagal diverifikasi',
                            'error'
                        )
                    }
                } else {
                    Toast.fire(
                        'Gagal',
                        'Member dengan id ' + id + ' gagal diverifikasi',
                        'error'
                    )
                }
            }
            xhr.send();
        }

        function rejectMember(id) {
            console.log(id);
            var url = "/admin/member/reject/";
            var xhr = new XMLHttpRequest();

            xhr.open("PUT", url + id, true);
            xhr.setRequestHeader("X-CSRF-TOKEN", "{{ csrf_token() }}");
            xhr.setRequestHeader("content-type", "application/json");
            xhr.onload = function() {
                var response = JSON.parse(xhr.response);
                if (xhr.readyState == 4 && xhr.status == "200") {
                    if (response.status == 200) {
                        var element = document.getElementById('pendingUser' + id + '');
                        element.parentNode.removeChild(element);
                        Toast.fire(
                            'Berhasil',
                            'Pengajuan member dengan id ' + id + ' berhasil ditolak',
                            'success'
                        )
                    } else {
                        Toast.fire(
                            'Gagal',
                            'Pengajuan Member dengan id ' + id + ' gagal ditolak',
                            'error'
                        )
                    }
                } else {
                    Toast.fire(
                        'Gagal',
                        'Member dengan id ' + id + ' gagal diverifikasi',
                        'error'
                    )
                }
            }
            xhr.send();
        }
    </script>
@endpush
