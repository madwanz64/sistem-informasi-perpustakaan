@extends('layouts.admin')

@section('pageTitle')
    Data Member | Sistem Informasi Perpustakaan
@endsection

@section('bodySection')
    <div class="">
        <div class="fs-5 fw-bold text-bg-success rounded-3 px-3 py-2 bg-gradient">Data Member</div>
        <hr>
        <div class="fs-5 fw-semibold text-uppercase text-center mb-3">Identitas Member</div>
        <dl class="row">
            <dt class="col-md-4 text-md-end">Nama Lengkap</dt>
            <dd class="col-md-8">{{ $user->name }}</dd>
            <dt class="col-md-4 text-md-end">Alamat E-mail</dt>
            <dd class="col-md-8">{{ $user->email }}</dd>
            <dt class="col-md-4 text-md-end">Nomor Induk Mahasiswa</dt>
            <dd class="col-md-8">{{ $profile->nim }}</dd>
            <dt class="col-md-4 text-md-end">Fakultas</dt>
            <dd class="col-md-8">{{ $profile->faculty_name }}</dd>
            <dt class="col-md-4 text-md-end">Jurusan</dt>
            <dd class="col-md-8">{{ $profile->major_name }}</dd>
            <dt class="col-md-4 text-md-end">Tahun Masuk</dt>
            <dd class="col-md-8">{{ $profile->entrance_year }}</dd>
            <dt class="col-md-4 text-md-end">Nomor Telepon</dt>
            <dd class="col-md-8">{{ $profile->phone_number }}</dd>
            <dt class="col-md-4 text-md-end">Alamat</dt>
            <dd class="col-md-8">{{ $profile->address }}</dd>
            <dt class="col-md-4 text-md-end">Status Member</dt>
            <dd class="col-md-8">{{ $user->is_member ? 'Member' : 'Belum diverifikasi Admin' }}</dd>
            <dt class="col-md-4 text-md-end">Tanggal Bergabung</dt>
            <dd class="col-md-8">{{ \Carbon\Carbon::parse($user->created_at)->format('d M Y H:i:s') }}
            </dd>
            <dt class="col-md-4 text-md-end">Terakhir Diperbarui</dt>
            <dd class="col-md-8">{{ \Carbon\Carbon::parse($user->updated_at)->format('d M Y H:i:s') }}
            </dd>
        </dl>
        <div class="fs-5 fw-semibold text-uppercase text-center mb-3">Ubah Data</div>
        <form action="{{ route('admin.member.update', ['member' => $user->id]) }}" method="post">
            @csrf
            @method('PUT')
            <div class="row">
                <div class="col-md-6">
                    <div class="mb-3">
                        <label for="valid_profile" class="form-label">Apa profile-nya valid?</label>
                        <select class="form-control" name="valid_profile" id="valid_profile">
                            <option value="1"
                                {{ (old('valid_profile') ? old('valid_profile') : $user->valid_profile) == 1 ? 'selected' : null }}>
                                Ya, profile valid</option>
                            <option value="0"
                                {{ (old('valid_profile') ? old('valid_profile') : $user->valid_profile) == 0 ? 'selected' : null }}>
                                Tidak, profile tidak valid</option>
                        </select>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="mb-3">
                        <label for="is_member" class="form-label">Status Member?</label>
                        <select class="form-control" name="is_member" id="is_member">
                            <option value="1"
                                {{ (old('is_member') ? old('is_member') : $user->is_member) == 1 ? 'selected' : null }}>
                                Member</option>
                            <option value="0"
                                {{ (old('is_member') ? old('is_member') : $user->is_member) == 0 ? 'selected' : null }}>
                                Bukan Member</option>
                        </select>
                    </div>
                </div>
            </div>
            <button type="submit" class="btn btn-success bg-gradient w-100">Ubah data member</button>
        </form>
    </div>
@endsection
