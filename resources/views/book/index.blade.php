@extends('layouts.app')

@section('pageTitle')
    Daftar Buku | Sistem Informasi Perpustakaan
@endsection

@section('bodySection')
    @include('layouts.components.searchbar')

    <div class="container">
        <div class="px-4 py-5">
            <div class="fs-2 fw-bold text-uppercase text-center mb-3">Daftar Buku</div>
            <div class="row">
                <div class="col-lg-9 col-xl-10 order-2 order-lg-1 border-start border-end">
                    @if (request()->search != null)
                        <div class="text-muted"><em>Menampilkan
                                {{ request()->entry == 6 || request()->entry == 12 || request()->entry == 24 || request()->entry == 48 ? request()->entry : 6 }}
                                buku {{ request()->borrowable == 1 ? 'yang dapat dipinjam' : null }} dengan judul
                                {{ request()->search }}</em></div>
                        <hr>
                    @endif
                    <div class="row">
                        @forelse ($books as $book)
                            <div class="col-xl-6 mb-3">
                                <div class="card h-100">
                                    <div class="row g-0 h-100">
                                        <div class="col-md-3 d-flex ps-3 py-3">
                                            @if ($book->cover_image != null)
                                                <img src="{{ asset('storage/' . $book->cover_image) }}"
                                                    style="max-height: 180px" class="img-fluid m-auto rounded-3"
                                                    alt="...">
                                            @else
                                                <img src="{{ asset('img/image-not-found.png') }}"
                                                    style="max-height: 180px" class="img-fluid m-auto rounded-3"
                                                    alt="...">
                                            @endif
                                        </div>
                                        <div class="col-md-9 d-flex flex-column justify-content-between">
                                            <div class="card-body">
                                                <div class="fs-5 fw-bold">{{ $book->title }}</div>
                                                <p class="text-muted">By {{ $book->author }}, Publisher :
                                                    {{ $book->publisher }}</p>
                                                <div class="row">
                                                    <div class="col-md-6 col-xl-12">
                                                        <div class="">Lokasi Buku</div>
                                                        <div class="text-muted">{{ $book->location }}</div>
                                                    </div>
                                                    <div class="col-md-6 col-xl-12">
                                                        <div class="">Sisa Eksemplar</div>
                                                        <div class="text-muted">{{ $book->amount_available }} dari
                                                            {{ $book->amount_total }} eksemplar</div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="card-footer bg-white mt-auto">
                                                <div class="row mt-3">
                                                    @auth
                                                        @if (auth()->user()->is_admin === 1)
                                                            <div class="col-md-6 mb-3">
                                                                <a name="borrow" id="borrow"
                                                                    class="btn btn-sm btn-success w-100 bg-gradient"
                                                                    href="{{ route('admin.book.edit', ['book' => $book->id]) }}"
                                                                    role="button">Edit Buku</a>
                                                            </div>
                                                        @else
                                                            @if ($book->borrowable === 0)
                                                                <div class="col-md-6 mb-3">
                                                                    <a name="borrow" id="borrow"
                                                                        class="btn btn-sm btn-secondary w-100 bg-gradient disabled"
                                                                        role="button">Hanya Baca</a>
                                                                </div>
                                                            @elseif ($book->amount_available === 0)
                                                                <div class="col-md-6 mb-3">
                                                                    <a name="borrow" id="borrow"
                                                                        class="btn btn-sm btn-secondary w-100 bg-gradient disabled"
                                                                        role="button">Stok Buku Habis</a>
                                                                </div>
                                                            @else
                                                                <div class="col-md-6 mb-3">
                                                                    <a name="borrow" id="borrow"
                                                                        class="btn btn-sm btn-success w-100 bg-gradient"
                                                                        role="button"
                                                                        onclick="addToCart({{ $book->id }})">Tambah ke
                                                                        Keranjang</a>
                                                                </div>
                                                            @endif
                                                        @endif
                                                    @endauth
                                                    @guest
                                                        @if ($book->borrowable === 0)
                                                            <div class="col-md-6 mb-3">
                                                                <a name="borrow" id="borrow"
                                                                    class="btn btn-sm btn-secondary w-100 bg-gradient disabled"
                                                                    role="button">Hanya Baca</a>
                                                            </div>
                                                        @elseif ($book->amount_available === 0)
                                                            <div class="col-md-6 mb-3">
                                                                <a name="borrow" id="borrow"
                                                                    class="btn btn-sm btn-secondary w-100 bg-gradient disabled"
                                                                    role="button">Stok Buku Habis</a>
                                                            </div>
                                                        @else
                                                            <div class="col-md-6 mb-3">
                                                                <a name="borrow" id="borrow"
                                                                    class="btn btn-sm btn-success w-100 bg-gradient"
                                                                    href="{{ route('login.index') }}" role="button">Pinjam
                                                                    Buku</a>
                                                            </div>
                                                        @endif
                                                    @endguest
                                                    <div class="col-md-6 mb-3">
                                                        <a name="detail" id="detail"
                                                            class="btn btn-sm btn-outline-success w-100 bg-gradient"
                                                            href="{{ route('book.show', ['book' => $book->id]) }}"
                                                            role="button">Detail Buku</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @empty
                            <div class="fs-5 fw-semibold text-center">Tidak dapat menemukan Buku</div>
                        @endforelse
                        {{ $books->links() }}
                    </div>
                </div>
                <div class="col-lg-3 col-xl-2 order-1 order-lg-2 border-start border-end">
                    <button class="btn btn-success bg-gradient d-lg-none mb-3 w-100" type="button"
                        data-bs-toggle="offcanvas" data-bs-target="#filter" aria-controls="filter">Filter</button>

                    <div class="offcanvas-lg offcanvas-end" tabindex="-1" id="filter" aria-labelledby="filterLabel">
                        <div class="offcanvas-header">
                            <h5 class="offcanvas-title text-uppercase" id="filterLabel">Filter</h5>
                            <button type="button" class="btn-close" data-bs-dismiss="offcanvas" data-bs-target="#filter"
                                aria-label="Close"></button>
                        </div>
                        <div class="offcanvas-body">
                            <div class="w-100">
                                <div class="fs-5 fw-bold text-uppercase text-center mb-3">Filter</div>
                                <form action="{{ route('book.index') }}" method="GET" name="search">
                                    <input type="hidden" name="search" value="{{ request()->search }}">
                                    <div class="form-check mb-3">
                                        <input type="checkbox" class="form-check-input" name="borrowable"
                                            id="borrowable" value="1"
                                            {{ request()->borrowable == 1 ? 'checked' : null }}>
                                        <label class="form-check-label" for="borrowable">
                                            Hanya tampilkan yang dapat dipinjam
                                        </label>
                                    </div>
                                    <div class="mb-3">
                                        <label for="entry" class="form-label fw-semibold">Jumlah data per
                                            halaman</label>
                                        <select class="form-select w-100" name="entry" id="entry">
                                            <option value="6" {{ request()->entry == 6 ? 'selected' : null }}>6
                                            </option>
                                            <option value="12" {{ request()->entry == 12 ? 'selected' : null }}>12
                                            </option>
                                            <option value="24" {{ request()->entry == 24 ? 'selected' : null }}>24
                                            </option>
                                            <option value="48" {{ request()->entry == 48 ? 'selected' : null }}>48
                                            </option>
                                        </select>
                                    </div>
                                    <button type="submit" class="btn btn-success bg-gradient w-100">Filter</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
@endsection


@push('script')
    <script>
        function addToCart(id) {
            console.log(id)
            var bookId = id;
            var url = "{{ route('cart.store') }}";
            var xhr = new XMLHttpRequest();

            let data = {
                'bookId': bookId
            }

            xhr.open("POST", url, true);
            xhr.setRequestHeader("X-CSRF-TOKEN", "{{ csrf_token() }}");
            xhr.setRequestHeader("content-type", "application/json");
            xhr.onload = function() {
                var response = JSON.parse(xhr.response);
                if (xhr.readyState == 4 && xhr.status == "200") {
                    if (response['status'] == 200) {
                        Toast.fire(
                            'Berhasil',
                            'Buku berhasil ditambahkan',
                            'success'
                        );
                    } else {
                        Toast.fire(
                            'Gagal',
                            response['message'],
                            'error'
                        );
                    }
                } else {
                    Toast.fire(
                        'Gagal',
                        'Buku gagal ditambahkan',
                        'error'
                    )
                }
            }
            xhr.send(JSON.stringify(data));
        }
    </script>
@endpush
