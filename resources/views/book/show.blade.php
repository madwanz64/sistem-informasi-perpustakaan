@extends('layouts.app')

@section('pageTitle')
    Judul Buku | Sistem Informasi Perpustakaan
@endsection

@section('bodySection')
    @include('layouts.components.searchbar')
    <div class="bg-light">
        <div class="container">
            <div class="px-4 py-5">
                <div class="row">
                    <div class="col-md-4 col-lg-3 mb-3">
                        <div class="card mb-3 pt-3">
                            @if ($book->cover_image != null)
                                <img src="{{ asset('storage/' . $book->cover_image) }}" style="max-height: 180px"
                                    class="img-fluid m-auto rounded-3" alt="...">
                            @else
                                <img src="{{ asset('img/image-not-found.png') }}" style="max-height: 180px"
                                    class="img-fluid m-auto rounded-3" alt="...">
                            @endif
                            <div class="card-body">
                                @auth
                                    @if (auth()->user()->is_admin === 1)
                                        <a name="borrow" id="borrow" class="btn btn-success w-100 bg-gradient mb-2"
                                            href="{{ route('admin.book.edit', ['book' => $book->id]) }}" role="button">Edit
                                            Buku</a>

                                        @if ($book->deleted_at != null)
                                            <form action="{{ route('admin.book.restore', ['book' => $book->id]) }}"
                                                method="post">
                                                @csrf
                                                <button type="submit"
                                                    class="btn btn-outline-success bg-gradient w-100">Munculkan
                                                    Buku</button>
                                            </form>
                                        @else
                                            <form action="{{ route('admin.book.destroy', ['book' => $book->id]) }}"
                                                method="post">
                                                @csrf
                                                @method('DELETE')
                                                <button type="submit"
                                                    class="btn btn-outline-secondary bg-gradient w-100">Sembunyikan
                                                    Buku</button>
                                            </form>
                                        @endif
                                    @else
                                        @if ($book->borrowable === 0)
                                            <a name="borrow" id="borrow" class="btn btn-secondary w-100 bg-gradient disabled"
                                                role="button">Hanya
                                                Baca</a>
                                        @elseif ($book->amount_available === 0)
                                            <a name="borrow" id="borrow" class="btn btn-secondary w-100 bg-gradient disabled"
                                                role="button">Stok
                                                Buku Habis</a>
                                        @else
                                            <a name="borrow" id="borrow" class="btn btn-success w-100 bg-gradient" role="button"
                                                onclick="addToCart({{ $book->id }})">Tambah ke Keranjang</a>
                                        @endif
                                    @endif
                                @endauth
                                @guest
                                    @if ($book->borrowable === 0)
                                        <a name="borrow" id="borrow" class="btn btn-secondary w-100 bg-gradient disabled"
                                            role="button">Hanya
                                            Baca</a>
                                    @elseif ($book->amount_available === 0)
                                        <a name="borrow" id="borrow" class="btn btn-secondary w-100 bg-gradient disabled"
                                            role="button">Stok
                                            Buku Habis</a>
                                    @else
                                        <a name="borrow" id="borrow" class="btn btn-success w-100 bg-gradient"
                                            href="{{ route('login.index') }}" role="button">Pinjam Buku</a>
                                    @endif
                                @endguest

                            </div>
                        </div>
                    </div>
                    <div class="col-md-8 col-lg-6 mb-3">
                        <div class="">
                            <div class="fs-3 fw-bold text-uppercase">{{ $book->title }}</div>
                            <div class="fs-5 text-muted">By {{ $book->author }}</div>
                            <hr>
                            @if ($bookDetail->synopsis != null)
                                <div class="mb-3">{{ $bookDetail->synopsis }}</div>
                            @else
                                <div class="mb-3">Tidak ada synopsis untuk buku ini</div>
                            @endif

                            <div class="text-muted">Published by {{ $book->publisher }} in
                                {{ $book->published_in_year }}</div>
                        </div>
                    </div>
                    <div class="col-lg-3 mb-3">
                        <div class="card">
                            <div class="card-body">
                                <dl class="row mb-0">
                                    <dt class="col-md-3 col-lg-12">Stok Buku</dt>
                                    <dd class="col-md-9 col-lg-12">
                                        {{ $book->amount_available . ' dari ' . $book->amount_total . ' eksemplar' }}
                                    </dd>
                                    <dt class="col-md-3 col-lg-12">Dapat Dipinjam?</dt>
                                    <dd class="col-md-9 col-lg-12">
                                        {{ $book->borrowable ? 'Ya, buku dapat dipinjam' : 'Hanya Baca' }}
                                    </dd>
                                </dl>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-6 mb-3">
                        <div class="card">
                            <div class="card-header text-bg-success bg-gradient">
                                <div class="fs-5">Detail Buku</div>
                            </div>
                            <div class="card-body">
                                <div class="fs-5 fw-bold text-muted">Data Utama</div>
                                <dl class="row">
                                    <dt class="col-md-3 col-lg-4 col-xl-3">Judul Buku</dt>
                                    <dd class="col-md-9 col-lg-8 col-xl-9">{{ $book->title }}</dd>
                                    <dt class="col-md-3 col-lg-4 col-xl-3">Penulis</dt>
                                    <dd class="col-md-9 col-lg-8 col-xl-9">{{ $book->author }}</dd>
                                    <dt class="col-md-3 col-lg-4 col-xl-3">Penerbit</dt>
                                    <dd class="col-md-9 col-lg-8 col-xl-9">{{ $book->publisher }}</dd>
                                    <dt class="col-md-3 col-lg-4 col-xl-3">Diterbitkan di</dt>
                                    <dd class="col-md-9 col-lg-8 col-xl-9">
                                        {{ $book->published_in_city . ', ' . $book->published_in_year }}</dd>
                                </dl>
                                @if ($bookDetail->format == null && $bookDetail->pagination == null && $bookDetail->number_of_pages == null && $bookDetail->dimensions == null)
                                @else
                                    <div class="fs-5 fw-bold text-muted">Data Fisik</div>
                                    <dl class="row">
                                        @if ($bookDetail->format != null)
                                            <dt class="col-md-3 col-lg-4 col-xl-3">Format</dt>
                                            <dd class="col-md-9 col-lg-8 col-xl-9">{{ $bookDetail->format }}</dd>
                                        @endif
                                        @if ($bookDetail->pagination != null)
                                            <dt class="col-md-3 col-lg-4 col-xl-3">Pagination</dt>
                                            <dd class="col-md-9 col-lg-8 col-xl-9">{{ $bookDetail->pagination }}</dd>
                                        @endif
                                        @if ($bookDetail->number_of_pages != null)
                                            <dt class="col-md-3 col-lg-4 col-xl-3">Number of pages</dt>
                                            <dd class="col-md-9 col-lg-8 col-xl-9">{{ $bookDetail->number_of_pages }}
                                            </dd>
                                        @endif
                                        @if ($bookDetail->dimensions != null)
                                            <dt class="col-md-3 col-lg-4 col-xl-3">Dimensions</dt>
                                            <dd class="col-md-9 col-lg-8 col-xl-9">{{ $bookDetail->dimensions }}</dd>
                                        @endif
                                    </dl>
                                @endif

                                @if ($bookDetail->work_description != null)
                                    <div class="fs-5 fw-bold text-muted">Deskripsi Buku</div>
                                    <p>{{ $bookDetail->work_description }}</p>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 mb-3">
                        <div class="card">
                            <div class="card-header text-bg-success bg-gradient">
                                <div class="fs-5">Status Buku</div>
                            </div>
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table table-hover">
                                        <thead>
                                            <tr>
                                                <th scope="col">#</th>
                                                <th scope="col">Ketersediaan</th>
                                                <th scope="col">Lokasi</th>
                                                <th scope="col">Status</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @if ($bookRequests->count() != 0)
                                                @foreach ($bookRequests as $key => $bookRequest)
                                                    <tr>
                                                        <th scope="row">{{ $key + 1 }}</th>
                                                        @if ($bookRequest->status == 'active')
                                                            <td class="text-nowrap">Sedang Dipinjam</td>
                                                            <td class="text-nowrap">Mahasiswa ID :
                                                                {{ $bookRequest->user_id }}</td>
                                                            <td class="text-nowrap">Dipinjam sejak
                                                                {{ \Carbon\Carbon::parse($bookRequest->updated_at)->format('d M Y') }}
                                                            </td>
                                                        @elseif ($bookRequest->status == 'approved')
                                                            <td class="text-nowrap">Sedang Dipinjam</td>
                                                            <td class="text-nowrap">Mahasiswa ID :
                                                                {{ $bookRequest->user_id }}</td>
                                                            <td class="text-nowrap">Menunggu untuk diambil</td>
                                                        @endif
                                                    </tr>
                                                @endforeach
                                                @if ($key + 1 < $book->amount_total)
                                                    <tr>
                                                        <th scope="row">{{ $key + 2 }} - {{ $book->amount_total }}
                                                        </th>
                                                        <td class="text-nowrap">Tersedia</td>
                                                        <td class="text-nowrap">{{ $book->location }}</td>
                                                        <td class="text-nowrap"> - </td>
                                                    </tr>
                                                @endif
                                            @else
                                                <tr>
                                                    <th scope="row">1 - {{ $book->amount_total }}
                                                    </th>
                                                    <td class="text-nowrap">Tersedia</td>
                                                    <td class="text-nowrap">{{ $book->location }}</td>
                                                    <td class="text-nowrap"> - </td>
                                                </tr>
                                            @endif
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('script')
    <script>
        function addToCart(id) {
            console.log(id)
            var bookId = id;
            var url = "{{ route('cart.store') }}";
            var xhr = new XMLHttpRequest();

            let data = {
                'bookId': bookId
            }

            xhr.open("POST", url, true);
            xhr.setRequestHeader("X-CSRF-TOKEN", "{{ csrf_token() }}");
            xhr.setRequestHeader("content-type", "application/json");
            xhr.onload = function() {
                var response = JSON.parse(xhr.response);
                if (xhr.readyState == 4 && xhr.status == "200") {
                    if (response['status'] == 200 && response['message'] == 'Buku berhasil ditambahkan ke keranjang') {
                        Swal.fire(
                            'Berhasil',
                            'Buku berhasil ditambahkan ke keranjang',
                            'success'
                        )
                    } else if (response['status'] == 400 && response['message'] ==
                        "Anda tidak dapat meminjam melebihi stok yang ada") {
                        Swal.fire(
                            'Gagal',
                            'Anda tidak dapat meminjam melebihi stok yang ada',
                            'error'
                        )
                    } else {
                        Swal.fire(
                            'Gagal',
                            'Buku gagal ditambahkan ke keranjang',
                            'error'
                        )
                    }
                } else {
                    Swal.fire(
                        'Gagal',
                        'Buku gagal ditambahkan ke keranjang',
                        'error'
                    )
                }
            }
            xhr.send(JSON.stringify(data));
        }
    </script>
@endpush
