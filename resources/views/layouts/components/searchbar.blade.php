<div class="bg-success bg-gradient">
    <div class="container">
        <div class="px-4 py-5">
            <form action="{{ route('book.index') }}" method="get" name="search">
                <div class="input-group mb-3 mx-auto" style="max-width: 540px">
                    <input type="text" class="form-control" name="search" value="{{ request()->search }}"
                        placeholder="Cari buku...">
                    <button class="btn btn-outline-light bg-gradient" type="submit"><i
                            class="fas fa-search"></i></button>
            </form>
        </div>
    </div>
</div>
</div>
