<nav class="navbar navbar-expand-lg bg-light bg-gradient fixed-top shadow-lg">
    <div class="container">
        <a class="navbar-brand text-success" href="{{ route('index') }}"><i class="fas fa-book fa-fw"></i> UNI
            E-Library</a>
        <button class="navbar-toggler d-lg-none" type="button" data-bs-toggle="offcanvas"
            data-bs-target="#offcanvasResponsive" aria-controls="offcanvasResponsive"><span
                class="navbar-toggler-icon"></span></button>
    </div>

</nav>
<nav class="navbar navbar-expand-md">
    <div class="container">
        <a class="navbar-brand text-light" href="{{ route('index') }}"><i class="fas fa-book fa-fw"></i> UNI
            E-Library</a>
    </div>
</nav>
