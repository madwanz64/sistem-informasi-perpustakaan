<div class="offcanvas-lg offcanvas-start" tabindex="-1" id="offcanvasResponsive"
    aria-labelledby="offcanvasResponsiveLabel">
    <div class="offcanvas-header">
        <h5 class="offcanvas-title" id="offcanvasResponsiveLabel">Menu</h5>
        <button type="button" class="btn-close" data-bs-dismiss="offcanvas" data-bs-target="#offcanvasResponsive"
            aria-label="Close"></button>
    </div>
    <div class="offcanvas-body">
        <div class="w-100">
            <div
                class="fw-semibold text-uppercase mb-3 d-none d-lg-block text-bg-success py-1 rounded-2 bg-gradient text-center">
                Menu</div>
            <nav class="nav nav-pills d-flex flex-column">
                <a class="nav-link text{{ str_contains(Route::currentRouteName(), 'member.index') ? '-bg' : null }}-success bg-gradient border-bottom"
                    href="{{ route('member.index') }}"><i class="fas fa-fw fa-user"></i> Profile</a>
                <a class="nav-link text{{ str_contains(Route::currentRouteName(), 'member.edit') ? '-bg' : null }}-success bg-gradient border-bottom"
                    href="{{ route('member.edit') }}"><i class="fas fa-fw fa-wrench"></i> Edit Profile</a>
                <a class="nav-link text{{ str_contains(Route::currentRouteName(), 'member.cart') ? '-bg' : null }}-success bg-gradient border-bottom"
                    href="{{ route('member.cart') }}"><i class="fas fa-fw fa-shopping-cart"></i>
                    Keranjang Peminjaman</a>
                <a class="nav-link text{{ str_contains(Route::currentRouteName(), 'member.borrowingstatus') ? '-bg' : null }}-success bg-gradient border-bottom"
                    href="{{ route('member.borrowingstatus') }}"><i class="fas fa-fw fa-list"></i> Status
                    Peminjaman</a>
                <a class="nav-link text{{ str_contains(Route::currentRouteName(), 'member.borrowinghistories') ? '-bg' : null }}-success bg-gradient border-bottom"
                    href="{{ route('member.borrowinghistories') }}"><i class="fas fa-fw fa-clipboard-list"></i>
                    Riwayat Peminjaman</a>
            </nav>
            <hr>
            <form action="{{ route('logout') }}" method="post">
                @csrf
                <button class="btn text-bg-danger bg-gradient w-100">Logout</button>
            </form>
        </div>
    </div>
</div>
