<nav class="navbar navbar-expand-md bg-light bg-gradient fixed-top shadow-lg">
    <div class="container">
        <a class="navbar-brand text-success" href="{{ route('index') }}"><i class="fas fa-book fa-fw"></i> UNI
            E-Library</a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav ms-auto mb-2 mb-lg-0">
                <hr>
                <li class="nav-item">
                    <a href="{{ route('book.index') }}" class="nav-link">Daftar Buku</a>
                </li>
                <hr>
                @guest
                    <li class="nav-item">
                        <a class="nav-link btn btn-success text-light bg-gradient"
                            href="{{ route('login.index') }}">Login</a>
                    </li>
                @endguest
                @auth
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button"
                            data-bs-toggle="dropdown" aria-expanded="false">
                            {{ auth()->user()->name }}
                        </a>
                        <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                            @if (auth()->user()->is_admin)
                                <li><a class="dropdown-item" href="{{ route('admin.dashboard') }}">Dashboard</a></li>
                            @else
                                <li><a class="dropdown-item" href="{{ route('member.index') }}">User Panel</a></li>
                                <li><a class="dropdown-item" href="{{ route('member.cart') }}">Keranjang
                                        Peminjaman</a>
                                </li>
                                <li><a class="dropdown-item" href="{{ route('member.borrowingstatus') }}">Status
                                        Peminjaman</a></li>
                            @endif
                            <li>
                                <hr class="dropdown-divider">
                            </li>
                            <li><a class="dropdown-item text-bg-success" href="{{ route('logout') }}"
                                    onclick="event.preventDefault();
                                                                                                                                                                                                                                                            document.getElementById('logout-form').submit();">
                                    {{ __('Logout') }} <i class="fa fa-sign-out"></i>
                                </a>
                                <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                    style="display: none;">
                                    @csrf
                                </form>
                            </li>
                        </ul>
                    </li>
                @endauth
            </ul>
        </div>
    </div>
</nav>
<nav class="navbar navbar-expand-md">
    <div class="container">
        <a class="navbar-brand text-success" href="{{ route('index') }}"><i class="fas fa-book fa-fw"></i> UNI
            E-Library</a>
    </div>
</nav>
