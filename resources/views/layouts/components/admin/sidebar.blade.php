<div class="offcanvas-lg offcanvas-start" tabindex="-1" id="offcanvasResponsive"
    aria-labelledby="offcanvasResponsiveLabel">
    <div class="offcanvas-header">
        <h5 class="offcanvas-title" id="offcanvasResponsiveLabel">Menu</h5>
        <button type="button" class="btn-close" data-bs-dismiss="offcanvas" data-bs-target="#offcanvasResponsive"
            aria-label="Close"></button>
    </div>
    <div class="offcanvas-body">
        <div class="w-100">
            <div
                class="fw-semibold text-uppercase mb-3 d-none d-lg-block text-bg-success py-1 rounded-2 bg-gradient text-center">
                Menu</div>
            <nav class="nav nav-pills d-flex flex-column">
                <a class="nav-link text{{ str_contains(Route::currentRouteName(), 'admin.dashboard') ? '-bg' : null }}-success bg-gradient border-bottom"
                    href="{{ route('admin.dashboard') }}"><i class="fas fa-fw fa-tachometer-alt"></i> Dashboard</a>
                <a class="nav-link text{{ str_contains(Route::currentRouteName(), 'admin.book.') ? '-bg' : null }}-success bg-gradient border-bottom"
                    href="#"><i class="fas fa-fw fa-book"></i>
                    Buku</a>
                <ul class="nav flex-column mb-0 ms-3">
                    <li><a class="nav-link text{{ str_contains(Route::currentRouteName(), 'admin.book.create') ? '-bg' : null }}-success bg-gradient border-bottom"
                            href="{{ route('admin.book.create') }}"><i class="fas fa-fw fa-book"></i> Tambah
                            Buku Baru</a></li>
                    <li><a class="nav-link text{{ str_contains(Route::currentRouteName(), 'admin.book.edit') ? '-bg' : null }}-success bg-gradient border-bottom"
                            href="{{ route('book.index') }}"><i class="fas fa-fw fa-book"></i> Data
                            Buku</a></li>
                </ul>
                <a class="nav-link text{{ str_contains(Route::currentRouteName(), 'admin.member') ? '-bg' : null }}-success bg-gradient border-bottom"
                    href="#"><i class="fas fa-fw fa-users"></i>
                    Member</a>
                <ul class="nav flex-column mb-0 ms-3">
                    <li><a class="nav-link text{{ str_contains(Route::currentRouteName(), 'admin.member.pending') ? '-bg' : null }}-success bg-gradient border-bottom"
                            href="{{ route('admin.member.pending') }}"><i class="fas fa-fw fa-user-clock"></i> Pending
                            Member</a></li>
                    <li><a class="nav-link text{{ str_contains(Route::currentRouteName(), 'admin.member.index') || str_contains(Route::currentRouteName(), 'admin.member.edit') ? '-bg' : null }}-success bg-gradient border-bottom"
                            href="{{ route('admin.member.index') }}"><i class="fas fa-fw fa-user-edit"></i> Data
                            Member</a></li>
                </ul>
                <a class="nav-link text{{ str_contains(Route::currentRouteName(), 'admin.bookborrowing') ? '-bg' : null }}-success bg-gradient border-bottom"
                    href="#"><i class="fas fa-fw fa-clipboard-list"></i> Peminjaman
                    Buku</a>
                <ul class="nav flex-column mb-0 ms-3">
                    <li><a class="nav-link text{{ str_contains(Route::currentRouteName(), 'admin.bookborrowing.request') ? '-bg' : null }}-success bg-gradient border-bottom"
                            href="{{ route('admin.bookborrowing.request') }}"><i
                                class="fas fa-fw fa-clipboard-check"></i>
                            Ajuan Peminjaman</a></li>
                    <li><a class="nav-link text{{ str_contains(Route::currentRouteName(), 'admin.bookborrowing.pickup') ? '-bg' : null }}-success bg-gradient border-bottom"
                            href="{{ route('admin.bookborrowing.pickup') }}"><i
                                class="fas fa-fw fa-hand-holding-medical"></i>
                            Pengambilan Buku</a></li>
                    <li><a class="nav-link text{{ str_contains(Route::currentRouteName(), 'admin.bookborrowing.drop') ? '-bg' : null }}-success bg-gradient border-bottom"
                            href="{{ route('admin.bookborrowing.drop') }}"><i class="fas fa-fw fa-exchange-alt"></i>
                            Pengembalian Buku</a></li>
                    <li><a class="nav-link text{{ str_contains(Route::currentRouteName(), 'admin.bookborrowing.histories') ? '-bg' : null }}-success bg-gradient border-bottom"
                            href="{{ route('admin.bookborrowing.histories') }}"><i
                                class="fas fa-fw fa-clipboard-list"></i>
                            Riwayat Peminjaman</a></li>
                </ul>

            </nav>
            <hr>
            <form action="{{ route('logout') }}" method="post">
                @csrf
                <button class="btn text-bg-danger bg-gradient w-100">Logout</button>
            </form>
        </div>
    </div>
</div>
