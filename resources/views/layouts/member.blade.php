<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
    <script src="https://kit.fontawesome.com/8a64f7f26c.js" crossorigin="anonymous"></script>
    <title>@yield('pageTitle')</title>
</head>

<body>
    @include('layouts.components.member.navbar')

    <div class="bg-success bg-gradient">
        <div class="container">
            <div class="px-4 py-3">
                <div class="fs-5 fw-bold text-light text-uppercase">User Panel</div>
            </div>
        </div>
    </div>


    <div class="container">
        <div class="px-4 py-5">
            @if (auth()->user()->valid_profile === 0)
                <div class="alert alert-danger" role="alert">
                    <strong>Anda belum melengkapi profil Anda, lengkapi sekarang dan mulai meminjam buku!</strong>
                </div>
            @elseif (auth()->user()->is_member === 0)
                <div class="alert alert-warning" role="alert">
                    <strong>Tunggu verifikasi dari admin untuk mulai meminjam buku</strong>
                </div>
            @endif
            <div class="row">
                <div class="col-lg-3 border-start border-end">
                    @include('layouts.components.member.sidebar')
                </div>
                <div class="col-lg-9 border-start border-end">
                    @yield('bodySection')
                </div>
            </div>
        </div>
    </div>
    @include('layouts.components.footer')
    <script src="{{ asset('js/app.js') }}"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11.4.17/dist/sweetalert2.all.min.js"></script>
    <script>
        const Toast = Swal.mixin({
            toast: true,
            position: 'bottom-end',
            showConfirmButton: false,
            timer: 3000,
            timerProgressBar: true,
            didOpen: (toast) => {
                toast.addEventListener('mouseenter', Swal.stopTimer)
                toast.addEventListener('mouseleave', Swal.resumeTimer)
            }
        })
    </script>
    @include('sweetalert::alert')
    @stack('script')
</body>

</html>
