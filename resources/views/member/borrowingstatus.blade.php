@extends('layouts.member')

@section('pageTitle')
    Status Peminjaman | Sistem Informasi Perpustakaan
@endsection

@section('bodySection')
    <div class="">
        <div class="fs-5 fw-bold text-bg-success rounded-3 px-3 py-2 bg-gradient">Status Peminjaman</div>
        <hr>
        @if ($bookBorrowingRequests->count() != 0)
            @foreach ($bookBorrowingRequests as $bookRequest)
                <div class="row mb-3">
                    <div class="col-3 col-md-2 col-xl-1">
                        @if ($bookRequest->book->cover_image != null)
                            <img src="{{ asset('storage/' . $bookRequest->book->cover_image) }}" class="img-fluid"
                                style="max-height: 100px" alt="">
                        @else
                            <img src="{{ asset('img/image-not-found.png') }}" class="img-fluid" style="max-height: 100px"
                                alt="">
                        @endif
                    </div>
                    <div class="col-9 col-md-10 col-xl-11">
                        <div class="fw-semibold">{{ $bookRequest->book->title }}</div>
                        <div class="d-none d-md-block">
                            <div class="text-muted">By {{ $bookRequest->book->author }}, Publisher :
                                {{ $bookRequest->book->publisher }}</div>
                        </div>
                        <div class="">Status : <b>{{ Str::upper($bookRequest->status) }}</b></div>
                        @if ($bookRequest->status == 'pending')
                            <div class="text-muted"> Pengajuan dikirim tanggal
                                {{ \Carbon\Carbon::parse(auth()->user()->created_at)->format('d M Y') }}</div>
                        @elseif ($bookRequest->status == 'approved')
                            <div class="text-muted"> Pengajuan diterima tanggal
                                {{ \Carbon\Carbon::parse(auth()->user()->created_at)->format('d M Y') }}. Ambil buku
                                sebelum
                                {{ \Carbon\Carbon::parse(auth()->user()->created_at)->addDays(3)->format('d M Y') }}</div>
                        @elseif ($bookRequest->status == 'active')
                            <div class="text-muted"> Buku dipinjam sejak
                                {{ \Carbon\Carbon::parse(auth()->user()->created_at)->format('d M Y') }}. Kembalikan
                                sebelum
                                {{ \Carbon\Carbon::parse(auth()->user()->created_at)->addDays(7)->format('d M Y') }}
                            </div>
                        @endif
                    </div>
                </div>
                <hr>
            @endforeach
            {{ $bookBorrowingRequests->links() }}
        @else
            <div class=" fw-semibold text-center">Tidak ada permintaan buku yang sedang diproses, diterima, ataupun buku
                yang sedang dipinjam</div>
        @endif
    </div>
@endsection

@push('script')
    <script>
        function addBook(id) {
            console.log(id)
            let bookId = id;
            let url = "{{ route('cart.add') }}";
            let xhr = new XMLHttpRequest();

            let data = {
                'bookId': bookId
            }

            let amountForm = document.getElementById('amount' + id);

            xhr.open("POST", url, true);
            xhr.setRequestHeader("X-CSRF-TOKEN", "{{ csrf_token() }}");
            xhr.setRequestHeader("content-type", "application/json");
            xhr.onload = function() {
                var response = JSON.parse(xhr.response);
                if (xhr.readyState == 4 && xhr.status == "200") {
                    if (response['status'] == 200) {
                        amountValue = amountForm.value;
                        amountForm.value = parseInt(amountValue) + parseInt(1);
                    }
                    alert(response['message'])
                } else {
                    alert(response);
                }
            }
            xhr.send(JSON.stringify(data));
        }

        function subtractBook(id) {
            console.log(id)
            var bookId = id;
            var url = "{{ route('cart.subtract') }}";
            var xhr = new XMLHttpRequest();

            let data = {
                'bookId': bookId
            }

            let amountForm = document.getElementById('amount' + id);

            xhr.open("POST", url, true);
            xhr.setRequestHeader("X-CSRF-TOKEN", "{{ csrf_token() }}");
            xhr.setRequestHeader("content-type", "application/json");
            xhr.onload = function() {
                var response = JSON.parse(xhr.response);
                if (xhr.readyState == 4 && xhr.status == "200") {
                    if (response['status'] == 200 && amountForm.value == 1) {
                        let cart = document.getElementById('book' + id)
                        cart.parentNode.removeChild(cart)
                    } else if (response['status'] == 200) {
                        amountForm.value = parseInt(amountForm.value - 1);
                    }
                    alert(response['message'])
                } else {
                    alert(response);
                }
            }
            xhr.send(JSON.stringify(data));
        }
    </script>
@endpush
