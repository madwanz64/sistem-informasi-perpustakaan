@extends('layouts.member')

@section('pageTitle')
    Riwayat Peminjaman | Sistem Informasi Perpustakaan
@endsection

@section('bodySection')
    <div class="">
        <div class="fs-5 fw-bold text-bg-success rounded-3 px-3 py-2 bg-gradient">Riwayat Peminjaman</div>
        <hr>
        @if ($bookBorrowingHistories->count() != 0)
            @foreach ($bookBorrowingHistories as $bookRequest)
                <div class="row mb-3">
                    <div class="col-3 col-md-2 col-xl-1">
                        @if ($bookRequest->book->cover_image != null)
                            <img src="{{ asset('storage/' . $bookRequest->book->cover_image) }}" class="img-fluid"
                                style="max-height: 100px" alt="">
                        @else
                            <img src="{{ asset('img/image-not-found.png') }}" class="img-fluid" style="max-height: 100px"
                                alt="">
                        @endif
                    </div>
                    <div class="col-9 col-md-10 col-xl-11">
                        <div class="fw-semibold">{{ $bookRequest->book->title }}</div>
                        <div class="d-none d-md-block">
                            <div class="text-muted">By {{ $bookRequest->book->author }}, Publisher :
                                {{ $bookRequest->book->publisher }}</div>
                        </div>
                        <div class="">Status : <b>{{ Str::upper($bookRequest->status) }}</b></div>
                        <div class="text-muted">
                            {{ \Carbon\Carbon::parse(auth()->user()->created_at)->format('d M Y') }}</div>
                    </div>
                </div>
                <hr>
            @endforeach
            {{ $bookBorrowingHistories->links() }}
        @else
            <div class=" fw-semibold text-center">Tidak ada riwayat peminjaman buku </div>
        @endif
    </div>
@endsection
