@extends('layouts.member')

@section('pageTitle')
    Keranjang Peminjaman | Sistem Informasi Perpustakaan
@endsection

@section('bodySection')
    <div class="">
        <div class="fs-5 fw-bold text-bg-success rounded-3 px-3 py-2 bg-gradient">Keranjang Peminjaman</div>
        <hr>
        <form action="{{ route('cart.request') }}" method="post">
            @csrf
            @if ($bookBorrowingCart->count() != 0)
                @foreach ($bookBorrowingCart as $book)
                    <div class="" id="book{{ $book->book_id }}">
                        <div class="row">
                            <div class="col-1 d-flex my-auto">
                                <div class="form-check mx-auto">
                                    <input class="form-check-input" type="checkbox" value="{{ $book->book_id }}"
                                        name="borrow[]">
                                </div>
                            </div>
                            <div class="col-3 col-md-2 col-xl-1">
                                @if ($book->book->cover_image != null)
                                    <img src="{{ asset('storage/' . $book->book->cover_image) }}" class="img-fluid"
                                        style="max-height: 100px" alt="">
                                @else
                                    <img src="{{ asset('img/image-not-found.png') }}" class="img-fluid"
                                        style="max-height: 100px" alt="">
                                @endif
                            </div>
                            <div class="col-7 col-md-9 col-xl-10">
                                <div class="fw-semibold">{{ $book->book->title }}</div>
                                <div class="d-none d-md-block">
                                    <div class="text-muted">By {{ $book->book->author }}, Publisher :
                                        {{ $book->book->publisher }}</div>
                                </div>
                                <div class="">Stok Buku Tersedia : {{ $book->book->amount_available }}
                                    eksemplar</div>
                                <div class="input-group" style="max-width: 200px">
                                    <button class="btn btn-outline-success" type="button"
                                        onclick="subtractBook({{ $book->book_id }})">{{ '<' }}</button>
                                    <input type="number" id="amount{{ $book->book_id }}"
                                        name="amount[{{ $book->book_id }}]" value="{{ $book->amount }}"
                                        class="form-control text-center" readonly>
                                    <button class="btn btn-outline-success" type="button"
                                        onclick="addBook({{ $book->book_id }})">{{ '>' }}</button>
                                </div>
                            </div>
                        </div>
                        <hr>
                    </div>
                @endforeach
                @if (auth()->user()->is_member)
                    <button type="submit" class="btn btn-success bg-gradient w-100">Pinjam Buku</button>
                @else
                    <button type="button" class="btn btn-secondary bg-gradient w-100 disabled">Pinjam Buku</button>
                @endif
            @else
                <div class=" fw-semibold text-center">Keranjang Anda masih kosong</div>
            @endif

        </form>
    </div>
@endsection

@push('script')
    <script>
        function addBook(id) {
            console.log(id)
            let bookId = id;
            let url = "{{ route('cart.add') }}";
            let xhr = new XMLHttpRequest();

            let data = {
                'bookId': bookId
            }

            let amountForm = document.getElementById('amount' + id);

            xhr.open("POST", url, true);
            xhr.setRequestHeader("X-CSRF-TOKEN", "{{ csrf_token() }}");
            xhr.setRequestHeader("content-type", "application/json");
            xhr.onload = function() {
                var response = JSON.parse(xhr.response);
                if (xhr.readyState == 4 && xhr.status == "200") {
                    if (response['status'] == 200) {
                        amountValue = amountForm.value;
                        amountForm.value = parseInt(amountValue) + parseInt(1);
                        Toast.fire(
                            'Berhasil',
                            'Buku berhasil ditambah',
                            'success'
                        )
                    } else if (response['status'] == 400 && response['message'] ==
                        'Anda tidak dapat meminjam melebihi stok yang ada') {
                        Toast.fire(
                            'Gagal',
                            'Anda tidak dapat meminjam melebihi stok yang ada',
                            'error'
                        )
                    } else {
                        Toast.fire(
                            'Gagal',
                            'Buku gagal ditambahkan',
                            'error'
                        )
                    }
                } else {
                    Toast.fire(
                        'Gagal',
                        'Buku gagal ditambahkan',
                        'error'
                    )
                }
            }
            xhr.send(JSON.stringify(data));
        }

        function subtractBook(id) {
            console.log(id)
            var bookId = id;
            var url = "{{ route('cart.subtract') }}";
            var xhr = new XMLHttpRequest();

            let data = {
                'bookId': bookId
            }

            let amountForm = document.getElementById('amount' + id);

            xhr.open("POST", url, true);
            xhr.setRequestHeader("X-CSRF-TOKEN", "{{ csrf_token() }}");
            xhr.setRequestHeader("content-type", "application/json");
            xhr.onload = function() {
                var response = JSON.parse(xhr.response);
                if (xhr.readyState == 4 && xhr.status == "200") {
                    if (response['status'] == 200 && amountForm.value == 1) {
                        let cart = document.getElementById('book' + id)
                        cart.parentNode.removeChild(cart)
                        Toast.fire(
                            'Berhasil',
                            'Buku berhasil dikurangi dari keranjang',
                            'success'
                        )
                    } else if (response['status'] == 200) {
                        amountForm.value = parseInt(amountForm.value - 1);
                        Toast.fire(
                            'Berhasil',
                            'Buku berhasil dikurangi dari keranjang',
                            'success'
                        )
                    } else {
                        Toast.fire(
                            'Gagal',
                            'Buku gagal dikurangi dari keranjang',
                            'error'
                        )
                    }
                } else {
                    Toast.fire(
                        'Gagal',
                        'Buku gagal dikurangi dari keranjang',
                        'error'
                    )
                }
            }
            xhr.send(JSON.stringify(data));
        }
    </script>
@endpush
