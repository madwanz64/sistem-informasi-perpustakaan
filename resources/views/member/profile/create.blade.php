@extends('layouts.member')

@section('pageTitle')
    Lengkapi Profile | Sistem Informasi Perpustakaan
@endsection

@section('bodySection')
    <div class="">
        <div class="fs-5 fw-bold text-bg-success rounded-3 px-3 py-2 bg-gradient">Lengkapi Profile</div>
        <hr>
        <form class="mx-auto" action="{{ route('member.store') }}" method="post" style="max-width: 540px">
            @csrf
            <div class="mb-3">
                <label for="name" class="form-label">Nama Lengkap <span class="text-danger fw-bold">*</span></label>
                <input type="text" class="form-control @error('name') is-invalid @enderror " name="name" id="name"
                    value="{{ old('name') ? old('name') : auth()->user()->name }}" placeholder="Nama Lengkap" required>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="mb-3">
                        <label for="email" class="form-label">E-Mail <span class="text-danger fw-bold">*</span></label>
                        <input type="text" class="form-control" id="email" value="{{ auth()->user()->email }}"
                            placeholder="Nama Lengkap" disabled>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="mb-3">
                        <label for="nim" class="form-label">Nomor Induk Mahasiswa <span
                                class="text-danger fw-bold">*</span></label>
                        <input type="text" class="form-control @error('nim') is-invalid @enderror " name="nim" id="nim"
                            value="{{ old('nim') ? old('nim') : '' }}" placeholder="Nomor Induk Mahasiswa" required>
                    </div>
                </div>
            </div>
            <div class="mb-3">
                <label for="faculty_id" class="form-label">Fakultas <span class="text-danger fw-bold">*</span></label>
                <select class="form-control @error('faculty_id') is-invalid @enderror" name="faculty_id" id="faculty_id"
                    required>
                    <option {{ old('faculty_id') ? null : 'selected' }} disabled>Pilih Fakultas</option>
                    @foreach ($faculties as $faculty)
                        <option value="{{ $faculty->id }}" {{ old('faculty_id') == $faculty->id ? 'selected' : null }}>
                            {{ $faculty->name . ' (' . $faculty->abbreviation . ')' }}
                        </option>
                    @endforeach
                </select>
            </div>
            <div class="mb-3">
                <label for="major_id" class="form-label">Jurusan <span class="text-danger fw-bold">*</span></label>
                <select class="form-control @error('major_id') is-invalid @enderror" name="major_id" id="major_id" required>
                    <option {{ old('major_id') ? null : 'selected' }} disabled>Pilih Jurusan</option>
                    @foreach ($majors as $major)
                        <option value="{{ $major->id }}" {{ old('major_id') == $major->id ? 'selected' : null }}>
                            {{ $major->name . ' (' . $major->abbreviation . ')' }}
                        </option>
                    @endforeach
                </select>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="mb-3">
                        <label for="entrance_year" class="form-label">Tahun Masuk <span
                                class="text-danger fw-bold">*</span></label>
                        <input type="number" class="form-control @error('entrance_year') is-invalid @enderror"
                            name="entrance_year" id="entrance_year" value="{{ old('entrance_year') }}"
                            placeholder="Tahun Masuk" required>
                        <small class="text-muted">Isi dengan tahun masuk kuliah</small>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="mb-3">
                        <label for="phone_number" class="form-label">Nomor Telepon <span
                                class="text-danger fw-bold">*</span></label>
                        <input type="text" class="form-control @error('phone_number') is-invalid @enderror"
                            name="phone_number" id="phone_number" value="{{ old('phone_number') }}"
                            placeholder="Nomor Telepon" required>
                        <small class="text-muted">Tulis nomor telepon dengan kode negara</small>
                    </div>
                </div>
            </div>
            <div class="mb-3">
                <label for="address" class="form-label">Alamat Lengkap <span
                        class="text-danger fw-bold">*</span></label>
                <textarea class="form-control @error('address') is-invalid @enderror" name="address" id="address" rows="3">{{ old('address') }}</textarea>
                <small class="text-muted">Isi alamat selengkap mungkin</small>
            </div>
            <button type="submit" class="btn btn-success bg-gradient w-100">Lengkapi Data</button>
        </form>
    </div>
@endsection
