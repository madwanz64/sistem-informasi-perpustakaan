@extends('layouts.member')

@section('pageTitle')
    {{ auth()->user()->name }} | Sistem Informasi Perpustakaan
@endsection

@section('bodySection')
    <div class="">
        <div class="fs-5 fw-bold text-bg-success rounded-3 px-3 py-2 bg-gradient">Profile</div>
        <hr>
        <div class="fs-5 fw-semibold text-uppercase text-center mb-3">Identitas Diri</div>
        <dl class="row">
            <dt class="col-md-4 text-md-end">Nama Lengkap</dt>
            <dd class="col-md-8">{{ auth()->user()->name }}</dd>
            <dt class="col-md-4 text-md-end">Alamat E-mail</dt>
            <dd class="col-md-8">{{ auth()->user()->email }}</dd>
            <dt class="col-md-4 text-md-end">Nomor Induk Mahasiswa</dt>
            <dd class="col-md-8">{{ $profile->nim }}</dd>
            <dt class="col-md-4 text-md-end">Fakultas</dt>
            <dd class="col-md-8">{{ $profile->faculty_name }}</dd>
            <dt class="col-md-4 text-md-end">Jurusan</dt>
            <dd class="col-md-8">{{ $profile->major_name }}</dd>
            <dt class="col-md-4 text-md-end">Tahun Masuk</dt>
            <dd class="col-md-8">{{ $profile->entrance_year }}</dd>
            <dt class="col-md-4 text-md-end">Nomor Telepon</dt>
            <dd class="col-md-8">{{ $profile->phone_number }}</dd>
            <dt class="col-md-4 text-md-end">Alamat</dt>
            <dd class="col-md-8">{{ $profile->address }}</dd>
            <dt class="col-md-4 text-md-end">Status Member</dt>
            <dd class="col-md-8">{{ auth()->user()->is_member ? 'Member' : 'Belum diverifikasi Admin' }}</dd>
            <dt class="col-md-4 text-md-end">Tanggal Bergabung</dt>
            <dd class="col-md-8">{{ \Carbon\Carbon::parse(auth()->user()->created_at)->format('d M Y H:i:s') }}
            </dd>
            <dt class="col-md-4 text-md-end">Terakhir Diperbarui</dt>
            <dd class="col-md-8">{{ \Carbon\Carbon::parse(auth()->user()->updated_at)->format('d M Y H:i:s') }}
            </dd>
        </dl>
    </div>
@endsection
