@extends('layouts.app')

@section('pageTitle')
    Register | Sistem Informasi Perpustakaan
@endsection

@section('bodySection')
    <div class="container">
        <div class="px-4 py-5">
            <div class="card mx-auto" style="max-width: 540px">
                <div class="card-header text-bg-success bg-gradient">
                    <div class="fs-2 fw-bold text-uppercase text-center">Register</div>
                </div>
                <div class="card-body">
                    <form action="{{ route('register.store') }}" method="post">
                        @csrf
                        <div class="mb-3">
                            <label for="name" class="form-label">Nama Lengkap</label>
                            <input type="text" class="form-control" id="name" name="name" required>
                        </div>
                        <div class="mb-3">
                            <label for="email" class="form-label">Email address</label>
                            <input type="email" class="form-control" id="email" name="email" required>
                            @error('email')
                                <div id="emailHelp" class="form-text text-danger">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="mb-3">
                            <label for="password" class="form-label">Password</label>
                            <input type="password" class="form-control" id="password" name="password" required>
                        </div>
                        <div class="mb-3">
                            <label for="password_confirmation" class="form-label">Password</label>
                            <input type="password" class="form-control" id="password_confirmation"
                                name="password_confirmation" required>
                        </div>
                        <button type="submit" class="btn btn-success w-100 mb-3">Register</button>
                        <a class="text-decoration-none text-center text-success d-block" href="{{ route('login.index') }}"
                            role="button">Sudah punya akun? Login</a>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
