@extends('layouts.app')

@section('pageTitle')
    Login | Sistem Informasi Perpustakaan
@endsection

@section('bodySection')
    <div class="container">
        <div class="px-4 py-5">
            <div class="card mx-auto" style="max-width: 540px">
                <div class="card-header text-bg-success bg-gradient">
                    <div class="fs-2 fw-bold text-uppercase text-center">Login</div>
                </div>
                <div class="card-body">
                    <form action="{{ route('login.authenticate') }}" method="post">
                        @csrf
                        <div class="mb-3">
                            <label for="email" class="form-label">Email address</label>
                            <input type="email" class="form-control" id="email" name="email" required>
                            @error('email')
                                <div id="emailHelp" class="form-text text-danger">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="mb-3">
                            <label for="password" class="form-label">Password</label>
                            <input type="password" class="form-control" id="password" name="password" required>
                        </div>
                        <button type="submit" class="btn btn-success w-100 mb-3">Login</button>
                        <a class="text-decoration-none text-center text-success d-block"
                            href="{{ route('register.index') }}" role="button">Belum punya akun? Daftar sekarang</a>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
