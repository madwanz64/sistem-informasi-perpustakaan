@extends('layouts.app')

@section('pageTitle')
    Sistem Informasi Perpustakaan
@endsection

@section('bodySection')
    <div class="bg-success bg-gradient">
        <div class="container">
            <div class="px-4 py-5 text-center text-white mx-auto" style="max-width: 810px">
                <i class="fas fa-book fa-fw fa-7x mb-3"></i>
                <div class="display-6 fw-bold text-uppercase">Sistem Informasi Perpustakaan</div>
                <p>Lorem, ipsum dolor sit amet consectetur adipisicing elit. Aliquid iure maiores
                    dolor, voluptates animi
                    perspiciatis facere at fugiat eveniet cumque optio quas nemo? Non deserunt in itaque repellat veniam,
                    facilis
                    delectus esse obcaecati ut minus cupiditate aperiam expedita laborum earum aliquam corporis
                    reprehenderit,
                    dicta
                    dolores? Sunt, facilis repellat. Ea, nihil?</p>
                <a class="btn bg-white text-success text-uppercase fw-semibold" href="{{ route('book.index') }}"
                    role="button">Lihat Daftar Buku</a>
            </div>
        </div>
    </div>
    {{-- <div class="container">
        <div class="px-4 py-5">
            <div class="fs-2 fw-bold text-uppercase text-center mb-3">Buku Terpopuler</div>
        </div>
    </div> --}}
@endsection
