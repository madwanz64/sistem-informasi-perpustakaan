<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use RealRashid\SweetAlert\Facades\Alert;

class AdminController extends Controller
{
    public function dashboard() {

        $amountBookTotal = DB::table('books')->sum('amount_total');
        $amountBookAvailable = DB::table('books')->sum('amount_available');
        $amountBookBorrowed = $amountBookTotal - $amountBookAvailable;
        $amountUser = DB::table('users')->where('is_admin', 0)->count();
        $amountMember = DB::table('users')->where('is_admin', 0)->where('is_member', 1)->count();
        $amountNonMember = $amountUser - $amountMember;
        $amountPendingRequest = DB::table('book_borrowing_requests')->where('status', 'pending')->count();
        $amountTotalBorrowRequest = DB::table('book_borrowing_requests')->wherein('status', ['active','returned'])->count();

        return view('admin.dashboard', [
            'amountBookTotal' => $amountBookTotal,
            'amountBookAvailable' => $amountBookAvailable,
            'amountBookBorrowed' => $amountBookBorrowed,
            'amountUser' => $amountUser,
            'amountMember' => $amountMember,
            'amountNonMember' => $amountNonMember,
            'amountPendingRequest' => $amountPendingRequest,
            'amountTotalBorrowRequest' => $amountTotalBorrowRequest
        ]);
    }

    public function dataMember() {

        $users = DB::table('users')->where('is_admin', 0)->where('is_member', 1)->where('deleted_at',null)->paginate(10);

        return view('admin.member.index', ['users' => $users]);
    }

    public function editMember($id) {
        
        $user = DB::table('users')->where('id', $id)->first();
        $profile = DB::table('profiles')->where('user_id', $id)->first();

        if($user == null || $profile == null) {
            abort(404);
        }

        return view('admin.member.edit', ['user' =>$user, 'profile' => $profile]);

    }

    public function updateMember(Request $request, $id) {
        $validatedData = $request->validate([
            'valid_profile' => 'required|integer|min:0|max:1',
            'is_member' => 'required|integer|min:0|max:1',
        ]);

        DB::table('users')->where('id', $id)->update([
            'valid_profile' => $validatedData['valid_profile'],
            'is_member' => $validatedData['is_member'],
            'updated_at' => Carbon::now()
        ]);

        Alert::success('Berhasil', 'Data User berhasil diperbarui');
        return redirect()->back();
    }

    public function deleteMember($id) {
        
        $user = DB::table('users')->where('id', $id)->first();

        if($user->is_admin === 1) {
            return response()->json([
                'status' => 400,
                'message' => 'Anda tidak dapat menghapus role admin'
            ]);
        }

        DB::table('users')->where('id', $id)->update([
            'deleted_at' => Carbon::now()
        ]);

        return response()->json([
            'status' => 200,
            'message' => 'User berhasil dinonaktifkan'
        ]);
    }

    public function pendingMember() {
        $pendingUsers = DB::table('users')->where('is_admin', 0)->where('valid_profile', 1)->where('is_member', 0)->get();

        $user_ids = [];
        foreach($pendingUsers as $key => $user) {
            $user_ids[$key] = $user->id;
        }

        $profiles = DB::table('profiles')->wherein('user_id', $user_ids)->get();

        foreach ($pendingUsers as $pendingUser) {
            foreach ($profiles as $profile) {
                if($pendingUser->id == $profile->user_id) {
                    $pendingUser->profile = $profile;
                }
            }
        }

        return view('admin.member.pending', ['pendingUsers' => $pendingUsers]);
    }

    public function verifyMember($id) {
        DB::table('users')->where('id', $id)->update(['is_member' => 1]);
        return response()->json([
            'status' => 200,
            'message' => "Member successfully verified"
        ]);
    }

    public function rejectMember($id) {
        DB::table('users')->where('id', $id)->update(['valid_profile' => 0]);
        return response()->json([
            'status' => 200,
            'message' => "Pengajuan member ditolak"
        ]);
    }

    public function bookBorrowingRequest() {
        $bookBorrowingRequests = DB::table('book_borrowing_requests')
                                ->where('status', 'pending')
                                ->paginate(10);

        $user_ids = [];
        $book_ids = [];
        foreach ($bookBorrowingRequests as $key => $value) {
            $book_ids[$key] = $value->book_id;
            $user_ids[$key] = $value->user_id;
        }
        
        $books = DB::table('books')->wherein('id', $book_ids)->get();
        $users = DB::table('users')->wherein('id', $user_ids)->get();

        foreach ($bookBorrowingRequests as $value) {
            foreach ($books as $book) {
                if($value->book_id == $book->id) {
                    $value->book = $book;
                }
            }
            foreach ($users as $user) {
                if($value->user_id == $user->id) {
                    $value->user = $user;
                }
            }
        }
        
        return view('admin.bookborrowing.request', ['bookBorrowingRequests' => $bookBorrowingRequests]);
    }

    public function bookBorrowingProcess(Request $request) {
        
        if($request->requestId == null) {
            Alert::error('Gagal', 'Tidak ada ajuan apapun yang ingin diproses');
            return redirect()->back();
        }

        $jumlahApprove = 0;
        $jumlahDisapprove = 0;
        $jumlahTotal = 0;
        $jumlahGagal = 0;

        foreach($request->requestId as $request_id => $value) {
            $jumlahTotal++;
            if($value == 1) {
                $bookBorrowingRequest = DB::table('book_borrowing_requests')->where('id', $request_id)->first();

                $book = DB::table('books')->where('id', $bookBorrowingRequest->book_id)->first();

                if($book->amount_available > 0) {
                    DB::table('book_borrowing_requests')
                    ->where('id', $request_id)
                    ->update([
                        'status' => 'approved',
                        'updated_at' => Carbon::now()
                    ]);

                    DB::table('books')->where('id', $bookBorrowingRequest->book_id)
                    ->update([
                        'amount_available' => $book->amount_available-1,
                        'updated_at' => Carbon::now()
                    ]);
                    $jumlahApprove++;
                } else {
                    $jumlahGagal++;
                }

            } elseif ($value == 0) {
                DB::table('book_borrowing_requests')
                ->where('id', $request_id)
                ->update([
                    'status' => 'disapproved',
                    'updated_at' => Carbon::now()
                ]);
                $jumlahDisapprove++;
            } 
        }

        if($jumlahGagal != 0) {
            Alert::warning('Peringatan', 'Ada ' . $jumlahGagal. " dari " . $jumlahTotal . " ajuan peminjaman yang gagal diproses");
            return redirect()->back();
        }

        Alert::success('Berhasil', 'Ajuan Peminjaman berhasil diproses');
        return redirect()->back();
    }

    public function bookBorrowingPickUp() {
        $bookBorrowingRequests = DB::table('book_borrowing_requests')
                                ->where('status', 'approved')
                                ->where('user_id', 'like', ((request()->user_id != null) ? request()->user_id : '%'))
                                ->paginate(((request()->user_id != null) ? 100 : 10))->withQueryString();

        $user_ids = [];
        $book_ids = [];
        foreach ($bookBorrowingRequests as $key => $value) {
            $book_ids[$key] = $value->book_id;
            $user_ids[$key] = $value->user_id;
        }
        
        $books = DB::table('books')->wherein('id', $book_ids)->get();
        $users = DB::table('users')->wherein('id', $user_ids)->get();

        foreach ($bookBorrowingRequests as $value) {
            foreach ($books as $book) {
                if($value->book_id == $book->id) {
                    $value->book = $book;
                }
            }
            foreach ($users as $user) {
                if($value->user_id == $user->id) {
                    $value->user = $user;
                }
            }
        }
        
        return view('admin.bookborrowing.pickup', ['bookBorrowingRequests' => $bookBorrowingRequests]);
    }

    public function bookBorrowingActive($id) {

        $bookBorrowingRequest = DB::table('book_borrowing_requests')->where('id', $id)->first();

        if($bookBorrowingRequest == null | $bookBorrowingRequest->status != 'approved') {
            return response()->json([
                'status' => 400,
                'message' => 'Bad Request'
            ]);
        }

        DB::table('book_borrowing_requests')
        ->where('id', $id)
        ->update([
            'status' => 'active',
            'updated_at' => Carbon::now()
        ]);

        return response()->json([
            'status' => 200,
            'message' => 'Success'
        ]);
    }

    public function bookBorrowingCancel($id) {
        $bookBorrowingRequest = DB::table('book_borrowing_requests')->where('id', $id)->first();

        if($bookBorrowingRequest == null | $bookBorrowingRequest->status != 'approved') {
            return response()->json([
                'status' => 400,
                'message' => 'Bad Request'
            ]);
        }

        $book = DB::table('books')
        ->where('id', $bookBorrowingRequest->book_id)
        ->first();

        if($book == null) {
            return response()->json([
                'status' => 400,
                'message' => 'Bad Request'
            ]);
        }

        DB::table('books')
        ->where('id', $bookBorrowingRequest->book_id)
        ->update([
            'amount_available' => $book->amount_available+1,
            'updated_at' => Carbon::now()
        ]);

        DB::table('book_borrowing_requests')
        ->where('id', $id)
        ->update([
            'status' => 'returned',
            'updated_at' => Carbon::now()
        ]);

        return response()->json([
            'status' => 200,
            'message' => 'Success'
        ]);
    }
    
    public function bookBorrowingDrop() {
        $bookBorrowingRequests = DB::table('book_borrowing_requests')
                                ->where('status', 'active')
                                ->where('user_id', 'like', ((request()->user_id != null) ? request()->user_id : '%'))
                                ->paginate(((request()->user_id != null) ? 100 : 10))->withQueryString();

        $user_ids = [];
        $book_ids = [];
        foreach ($bookBorrowingRequests as $key => $value) {
            $book_ids[$key] = $value->book_id;
            $user_ids[$key] = $value->user_id;
        }
        
        $books = DB::table('books')->wherein('id', $book_ids)->get();
        $users = DB::table('users')->wherein('id', $user_ids)->get();

        foreach ($bookBorrowingRequests as $value) {
            foreach ($books as $book) {
                if($value->book_id == $book->id) {
                    $value->book = $book;
                }
            }
            foreach ($users as $user) {
                if($value->user_id == $user->id) {
                    $value->user = $user;
                }
            }
        }
        
        return view('admin.bookborrowing.drop', ['bookBorrowingRequests' => $bookBorrowingRequests]);
    }


    public function bookBorrowingReturn($id) {
        $bookBorrowingRequest = DB::table('book_borrowing_requests')->where('id', $id)->first();

        if($bookBorrowingRequest == null | $bookBorrowingRequest->status != 'active') {
            return response()->json([
                'status' => 400,
                'message' => 'Bad Request'
            ]);
        }

        $book = DB::table('books')
        ->where('id', $bookBorrowingRequest->book_id)
        ->first();

        if($book == null) {
            return response()->json([
                'status' => 400,
                'message' => 'Bad Request'
            ]);
        }

        DB::table('books')
        ->where('id', $bookBorrowingRequest->book_id)
        ->update([
            'amount_available' => $book->amount_available+1,
            'updated_at' => Carbon::now()
        ]);

        DB::table('book_borrowing_requests')
        ->where('id', $id)
        ->update([
            'status' => 'returned',
            'updated_at' => Carbon::now()
        ]);

        return response()->json([
            'status' => 200,
            'message' => 'Success'
        ]);
    }

    public function bookBorrowingHistories() {
        $bookBorrowingHistories = DB::table('book_borrowing_requests')
                                ->orderBy('id', 'desc')
                                ->paginate(5);
        
        $book_ids = [];
        $user_ids = [];
        foreach ($bookBorrowingHistories as $key => $value) {
            $book_ids[$key] = $value->book_id;
            $user_ids[$key] = $value->user_id;
        }
        
        $books = DB::table('books')->wherein('id', $book_ids)->get();
        $users = DB::table('users')->wherein('id', $user_ids)->get();

        foreach ($bookBorrowingHistories as $value) {
            foreach ($books as $book) {
                if($value->book_id == $book->id) {
                    $value->book = $book;
                }
            }
            foreach ($users as $user) {
                if($value->user_id == $user->id) {
                    $value->user = $user;
                }
            }
        }

        return view('admin.bookborrowing.histories', ['bookBorrowingHistories' => $bookBorrowingHistories]);
    }
}
