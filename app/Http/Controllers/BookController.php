<?php

namespace App\Http\Controllers;

use App\Models\Book;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use RealRashid\SweetAlert\Facades\Alert;

class BookController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $books = DB::table('books')
                ->where('deleted_at', null)
                ->where('title', 'like', '%'.((request()->search != null) ? request()->search : '').'%')
                ->where('borrowable','like', ((request()->borrowable != null) ? request()->borrowable : '%'))
                ->orderBy('created_at', 'desc')->paginate(((request()->entry == 6 || request()->entry == 12 || request()->entry == 24 || request()->entry == 48) ? request()->entry : 6))->withQueryString();
        return view('book.index', ['books' => $books]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.book.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'title' => 'required|string',
            'author' => 'required|string',
            'publisher' => 'required|string',
            'published_in_city' => 'required|string',
            'published_in_year' => 'required|integer',
            'location' => 'required|string',
            'cover_image' => 'nullable|image|file',
            'amount_total' => 'required|integer|min:0',
            'borrowable' => 'required|integer|min:0|max:1',
            'format' => 'nullable|string',
            'pagination' => 'nullable|string',
            'number_of_pages' => 'nullable|integer',
            'dimensions' => 'nullable|string',
            'language' => 'nullable|string',
            'synopsis' => 'nullable|string',
            'work_description' => 'nullable|string',
        ]);

        if($request->file('cover_image')) {
            $validatedData['cover_image'] = $request->file('cover_image')->store('cover-images');
        }
        // dd($validatedData['cover_image']);
        
        $id = DB::table('books')->insertGetId([
            'title' => $validatedData['title'],
            'author' => $validatedData['author'],
            'publisher' => $validatedData['publisher'],
            'published_in_city' => $validatedData['published_in_city'],
            'published_in_year' => $validatedData['published_in_year'],
            'location' => $validatedData['location'],
            'cover_image' => ($request->file('cover_image')) ? $validatedData['cover_image'] : null ,
            'amount_available' => $validatedData['amount_total'],
            'amount_total' => $validatedData['amount_total'],
            'borrowable' => $validatedData['borrowable'],
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);

        DB::table('details_of_books')->insert([
            'book_id' => $id,
            'format' => ($request->format) ? $validatedData['format'] : null,
            'pagination' => ($request->pagination) ? $validatedData['pagination'] : null,
            'number_of_pages' => ($request->number_of_pages) ? $validatedData['number_of_pages'] : null,
            'dimensions' => ($request->dimensions) ? $validatedData['dimensions'] : null,
            'language' => ($request->language) ? $validatedData['language'] : null,
            'synopsis' => ($request->synopsis) ? $validatedData['synopsis'] : null,
            'work_description' => ($request->work_description) ? $validatedData['work_description'] : null,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);

        Alert::success('Berhasil', 'Buku baru berhasil ditambahkan');
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Book  $book
     * @return \Illuminate\Http\Response
     */
    // public function show(Book $book)
    public function show($id)
    {
        $book = DB::table('books')->where('id', $id)->first();
        if(Auth::user() != null) {
            if($book == null || $book->deleted_at != null && auth()->user()->is_admin === 0) {
                abort(404);
            }
        } else {
            if($book == null || $book->deleted_at != null) {
                abort(404);
            }
        }
        
        $bookDetail = DB::table('details_of_books')->where('book_id', $id)->first();
        $bookRequests = DB::table('book_borrowing_requests')->where('book_id', $id)->wherein('status', ['approved', 'active'])->get();
        return view('book.show', ['book' => $book, 'bookDetail' => $bookDetail, 'bookRequests' => $bookRequests]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Book  $book
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $book = DB::table('books')->where('id', $id)->first();
        $bookDetail = DB::table('details_of_books')->where('book_id', $id)->first();

        return view('admin.book.edit', ['book' => $book, 'bookDetail' => $bookDetail]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Book  $book
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validatedData = $request->validate([
            'title' => 'required|string',
            'author' => 'required|string',
            'publisher' => 'required|string',
            'published_in_city' => 'required|string',
            'published_in_year' => 'required|integer',
            'location' => 'required|string',
            'cover_image' => 'nullable|image|file',
            'amount_total' => 'required|integer|min:0',
            'borrowable' => 'required|integer|min:0|max:1',
            'format' => 'nullable|string',
            'pagination' => 'nullable|string',
            'number_of_pages' => 'nullable|integer',
            'dimensions' => 'nullable|string',
            'language' => 'nullable|string',
            'synopsis' => 'nullable|string',
            'work_description' => 'nullable|string',
        ]);

        $book = DB::table('books')->where('id', $id)->first();

        if(($book->amount_available + ($validatedData['amount_total'] - $book->amount_total)) < 0) {
            Alert::error('Gagal', 'Jumlah buku tersedia harus lebih dari jumlah buku yang sedang dipinjam');
            return redirect()->back()->withErrors(['amount_total'=>'Jumlah buku tersedia harus lebih dari jumlah buku yang sedang dipinjam']);
        }
        $validatedData['amount_available'] = $book->amount_available + ($validatedData['amount_total'] - $book->amount_total);

        if($request->file('cover_image')) {
            Storage::delete($book->cover_image);
            $validatedData['cover_image'] = $request->file('cover_image')->store('cover-images');
        } else {
            $validatedData['cover_image'] = $book->cover_image;
        }

        DB::table('books')->where('id', $id)->update([
            'title' => $validatedData['title'],
            'author' => $validatedData['author'],
            'publisher' => $validatedData['publisher'],
            'published_in_city' => $validatedData['published_in_city'],
            'published_in_year' => $validatedData['published_in_year'],
            'location' => $validatedData['location'],
            'cover_image' => $validatedData['cover_image'],
            'amount_available' => $validatedData['amount_available'],
            'amount_total' => $validatedData['amount_total'],
            'borrowable' => $validatedData['borrowable'],
            'updated_at' => Carbon::now(),
        ]);

        DB::table('details_of_books')->where('book_id', $id)->update([
            'book_id' => $id,
            'format' => ($request->format) ? $validatedData['format'] : null,
            'pagination' => ($request->pagination) ? $validatedData['pagination'] : null,
            'number_of_pages' => ($request->number_of_pages) ? $validatedData['number_of_pages'] : null,
            'dimensions' => ($request->dimensions) ? $validatedData['dimensions'] : null,
            'language' => ($request->language) ? $validatedData['language'] : null,
            'synopsis' => ($request->synopsis) ? $validatedData['synopsis'] : null,
            'work_description' => ($request->work_description) ? $validatedData['work_description'] : null,
            'updated_at' => Carbon::now(),
        ]);

        Alert::success('Berhasil', 'Buku berhasil diperbarui');
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Book  $book
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $book = DB::table('books')->where('id', $id)->first();

        if($book->cover_image != null) {
            Storage::delete($book->cover_image);
        }

        DB::table('books')->where('id', $id)->update([
            'cover_image' => null,
            'deleted_at' => Carbon::now()
        ]);

        DB::table('details_of_books')->where('book_id', $id)->update([
            'deleted_at' => Carbon::now()
        ]);
        
        Alert::success('Berhasil', 'Buku berhasil disembunyikan');
        return redirect()->back();
    }

    public function restore($id) {
        DB::table('books')->where('id', $id)->update([
            'deleted_at' => null
        ]);

        DB::table('details_of_books')->where('book_id', $id)->update([
            'deleted_at' => null
        ]);

        Alert::success('Berhasil', 'Buku berhasil dikembalikan');
        return redirect()->back();
    }
}
