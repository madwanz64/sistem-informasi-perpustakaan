<?php

namespace App\Http\Controllers;

use App\Models\BookBorrowingCart;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use RealRashid\SweetAlert\Facades\Alert;

class BookBorrowingCartController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $bookBorrowingCart = DB::table('book_borrowing_carts')
                            ->where('user_id', auth()->user()->id)
                            ->get();

        $book_ids = [];
        foreach ($bookBorrowingCart as $key => $value) {
            $book_ids[$key] = $value->book_id;
        }
        
        $books = DB::table('books')->wherein('id', $book_ids)->get();

        foreach ($bookBorrowingCart as $value) {
            foreach ($books as $book) {
                if($value->book_id == $book->id) {
                    $value->book = $book;
                }
            }
        }
        
        return view('member.borrowingcart', ['bookBorrowingCart' => $bookBorrowingCart]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $book = DB::table('books')
                ->where('id', $request->bookId)
                ->where('deleted_at', null)
                ->first();

        if($book == null) {
            return response()->json([
                'status' => 400,
                'message' => 'Buku yang ditambahkan ke keranjang tidak valid'
            ]);
        }

        if($book->amount_available == 0) {
            return response()->json([
                'status' => 400,
                'message' => 'Stok buku habis'
            ]);
        }

        $bookInCart = DB::table('book_borrowing_carts')
                    ->where('user_id', auth()->user()->id)
                    ->where('book_id', $request->bookId)
                    ->first();
        
        if($bookInCart == null) {
            DB::table('book_borrowing_carts')
            ->insert([
                'user_id' => auth()->user()->id,
                'book_id' => $request->bookId,
                'amount' => 1,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ]);
        } else {
            if($bookInCart->amount >= $book->amount_available) {
                return response()->json([
                    'status' => 400,
                    'message' => 'Anda tidak dapat meminjam melebihi stok yang ada'
                ]);
            } else {
                DB::table('book_borrowing_carts')
                ->where('user_id', auth()->user()->id)
                ->where('book_id', $request->bookId)
                ->update([
                'amount' => $bookInCart->amount + 1,
                'updated_at' => Carbon::now()
            ]);
            }
        }
        
        return response()->json([
            'status' => 200,
            'message' => 'Buku berhasil ditambahkan ke keranjang'
        ]);
    }

    public function add(Request $request) 
    {
        $book = DB::table('books')
                ->where('id', $request->bookId)
                ->where('deleted_at', null)
                ->first();

        if($book == null) {
            return response()->json([
                'status' => 400,
                'message' => 'Buku yang akan ditambahkan tidak valid'
            ]);
        }

        if($book->amount_available == 0) {
            return response()->json([
                'status' => 400,
                'message' => 'Stok buku habis'
            ]);
        }

        $bookInCart = DB::table('book_borrowing_carts')
                    ->where('user_id', auth()->user()->id)
                    ->where('book_id', $request->bookId)
                    ->first();
        
        if($bookInCart == null) {
            DB::table('book_borrowing_carts')
            ->insert([
                'user_id' => auth()->user()->id,
                'book_id' => $request->bookId,
                'amount' => 1,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ]);
        } else {
            if($bookInCart->amount >= $book->amount_available) {
                return response()->json([
                    'status' => 400,
                    'message' => 'Anda tidak dapat meminjam melebihi stok yang ada'
                ]);
            } else {
                DB::table('book_borrowing_carts')
                ->where('user_id', auth()->user()->id)
                ->where('book_id', $request->bookId)
                ->update([
                'amount' => $bookInCart->amount + 1,
                'updated_at' => Carbon::now()
            ]);
            }
        }
        
        return response()->json([
            'status' => 200,
            'message' => 'Buku berhasil ditambahkan'
        ]);
    }

    public function subtract(Request $request) 
    {
        $book = DB::table('books')
                ->where('id', $request->bookId)
                ->where('deleted_at', null)
                ->first();

        if($book == null) {
            return response()->json([
                'status' => 400,
                'message' => 'Buku yang akan dikurangi dari keranjang tidak valid'
            ]);
        }

        $bookInCart = DB::table('book_borrowing_carts')
                    ->where('user_id', auth()->user()->id)
                    ->where('book_id', $request->bookId)
                    ->first();
        
        if($bookInCart == null) {
            return response()->json([
                'status' => 400,
                'message' => 'Tidak ada buku yang dimaksud di keranjang'
            ]);
        } else {
            if($bookInCart->amount == 1) {
                DB::table('book_borrowing_carts')
                ->where('user_id', auth()->user()->id)
                ->where('book_id', $request->bookId)
                ->delete();
            } else {
                DB::table('book_borrowing_carts')
                ->where('user_id', auth()->user()->id)
                ->where('book_id', $request->bookId)
                ->update([
                'amount' => $bookInCart->amount - 1,
                'updated_at' => Carbon::now()
                ]);
            }
        }
        
        return response()->json([
            'status' => 200,
            'message' => 'Buku berhasil dikurangi dari keranjang'
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\BookBorrowingCart  $bookBorrowingCart
     * @return \Illuminate\Http\Response
     */
    public function destroy(BookBorrowingCart $bookBorrowingCart)
    {
        //
    }
}
