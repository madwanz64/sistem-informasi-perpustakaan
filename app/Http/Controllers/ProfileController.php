<?php

namespace App\Http\Controllers;

use App\Models\Profile;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use RealRashid\SweetAlert\Facades\Alert;

class ProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $profile = DB::table('profiles')->where('user_id', auth()->user()->id)->first();

        if($profile == null) {
            return redirect()->route('member.create');
        }

        $profile = DB::table('profiles')->where('user_id', auth()->user()->id)->first();

        return view('member.profile.index', ['profile' => $profile]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $profile = DB::table('profiles')->where('user_id', auth()->user()->id)->first();

        if($profile != null) {
            return redirect()->route('member.index');
        }

        $faculties = DB::table('faculties')->get();
        $majors = DB::table('majors')->get();
        return view('member.profile.create', ['faculties' => $faculties, 'majors' => $majors]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'name' => 'required|string',
            'nim' => 'required|string',
            'faculty_id' => 'required|integer',
            'major_id' => 'required|integer',
            'entrance_year' => 'required|integer|min:0',
            'phone_number' => 'required|integer',
            'address' => 'required|string'
        ]);
        
        
        $faculty = DB::table('faculties')->where('id', $validatedData['faculty_id'])->first();
        $major = DB::table('majors')->where('id', $validatedData['major_id'])->first();
        if($faculty == null || $major == null || $major->faculty_id != $faculty->id) {
            return redirect()->back()->withErrors(['major_id' => 'Jurusan yang dipilih tidak sesuai dengan fakultas yang dipilih', 'faculty_id' => 'Fakultas yang dipilih tidak valid']);
        }

        $faculty_name = ($faculty->abbreviation != null) ? $faculty->name . " (". $faculty->abbreviation.")" : $faculty_name;
        $major_name = ($major->abbreviation != null) ? $major->name . " (". $major->abbreviation.")" : $major_name;

        DB::table('users')->where('id', auth()->user()->id)->update([
            'name' => $validatedData['name'],
            'valid_profile' => 1,
            'updated_at' => Carbon::now()
        ]);

        DB::table('profiles')->upsert([
            'user_id' => auth()->user()->id,
            'nim' => $validatedData['nim'],
            'faculty_id' => $faculty->id,
            'faculty_name' => $faculty_name,
            'major_id' => $major->id,
            'major_name' => $major_name,
            'entrance_year' => $validatedData['entrance_year'],
            'phone_number' => $validatedData['phone_number'],
            'address' => $validatedData['address'],
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ], [
            'user_id'
        ], [
            'nim', 
            'faculty_id', 
            'faculty_name', 
            'major_id', 
            'major_name', 
            'entrance_year',
            'phone_number',
            'address',
            'updated_at'
        ]);

        Alert::success('Berhasil', 'Profile berhasil diperbarui');
        return redirect(route('member.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Profile  $profile
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return view('member.profile.show');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Profile  $profile
     * @return \Illuminate\Http\Response
     */
    public function edit()
    {
        $profile = DB::table('profiles')->where('user_id', auth()->user()->id)->first();

        if($profile == null) {
            return redirect()->route('member.create');
        }
        
        $faculties = DB::table('faculties')->get();
        $majors = DB::table('majors')->get();
        return view('member.profile.edit', ['faculties' => $faculties, 'majors' => $majors, 'profile' => $profile]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Profile  $profile
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $validatedData = $request->validate([
            'name' => 'required|string',
            'nim' => 'required|string',
            'faculty_id' => 'required|integer',
            'major_id' => 'required|integer',
            'entrance_year' => 'required|integer|min:0',
            'phone_number' => 'required|integer',
            'address' => 'required|string'
        ]);
        
        
        $faculty = DB::table('faculties')->where('id', $validatedData['faculty_id'])->first();
        $major = DB::table('majors')->where('id', $validatedData['major_id'])->first();
        if($faculty == null || $major == null || $major->faculty_id != $faculty->id) {
            return redirect()->back()->withErrors(['major_id' => 'Jurusan yang dipilih tidak sesuai dengan fakultas yang dipilih', 'faculty_id' => 'Fakultas yang dipilih tidak valid']);
        }

        $faculty_name = ($faculty->abbreviation != null) ? $faculty->name . " (". $faculty->abbreviation.")" : $faculty_name;
        $major_name = ($major->abbreviation != null) ? $major->name . " (". $major->abbreviation.")" : $major_name;

        DB::table('users')->where('id', auth()->user()->id)->update([
            'name' => $validatedData['name'],
            'valid_profile' => 1,
            'updated_at' => Carbon::now()
        ]);

        DB::table('profiles')->where('user_id', auth()->user()->id)->update([
            'nim' => $validatedData['nim'],
            'faculty_id' => $faculty->id,
            'faculty_name' => $faculty_name,
            'major_id' => $major->id,
            'major_name' => $major_name,
            'entrance_year' => $validatedData['entrance_year'],
            'phone_number' => $validatedData['phone_number'],
            'address' => $validatedData['address'],
            'updated_at' => Carbon::now()
        ]);
        
        Alert::success('Berhasil', 'Profile berhasil diperbarui');
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Profile  $profile
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
