<?php

namespace App\Http\Controllers;

use App\Models\BookBorrowingRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use RealRashid\SweetAlert\Facades\Alert;


class BookBorrowingRequestController extends Controller
{
    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if($request->borrow == null || auth()->user()->is_member === 0) {
            Alert::error('Gagal', 'Permintaan peminjaman buku tidak dapat diproses');
            return redirect()->back();
        }

        foreach($request->borrow as $book_id) {
            for($i = 1; $i <= $request->amount[$book_id]; $i++) {
                DB::table('book_borrowing_requests')
                ->insert([
                    'user_id' => auth()->user()->id,
                    'book_id' => $book_id,
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now()
                ]);
            }

            DB::table('book_borrowing_carts')
            ->where('user_id', auth()->user()->id)
            ->where('book_id', $book_id)
            ->delete();
        }

        Alert::success('Berhasil', 'Permintaan peminjaman buku berhasil dikirim');
        return redirect()->back();
    }

    public function status() {
        $bookBorrowingRequests = DB::table('book_borrowing_requests')
                                ->where('user_id', auth()->user()->id)
                                ->wherein('status', ['pending','approved','active'])
                                ->paginate(5);
        
        $book_ids = [];
        foreach ($bookBorrowingRequests as $key => $value) {
            $book_ids[$key] = $value->book_id;
        }
        
        $books = DB::table('books')->wherein('id', $book_ids)->get();

        foreach ($bookBorrowingRequests as $value) {
            foreach ($books as $book) {
                if($value->book_id == $book->id) {
                    $value->book = $book;
                }
            }
        }

        return view('member.borrowingstatus', ['bookBorrowingRequests' => $bookBorrowingRequests]);
    }

    public function histories() {
        $bookBorrowingHistories = DB::table('book_borrowing_requests')
                                ->where('user_id', auth()->user()->id)
                                ->paginate(5);
        
        $book_ids = [];
        foreach ($bookBorrowingHistories as $key => $value) {
            $book_ids[$key] = $value->book_id;
        }
        
        $books = DB::table('books')->wherein('id', $book_ids)->get();

        foreach ($bookBorrowingHistories as $value) {
            foreach ($books as $book) {
                if($value->book_id == $book->id) {
                    $value->book = $book;
                }
            }
        }

        return view('member.borrowinghistories', ['bookBorrowingHistories' => $bookBorrowingHistories]);
    }
}
