<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AdminController;
use App\Http\Controllers\BookController;
use App\Http\Controllers\BookBorrowingCartController;
use App\Http\Controllers\BookBorrowingRequestController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\Auth\RegisterController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
})->name('index');

Route::get('book', [BookController::class, 'index'])->name('book.index');
Route::get('book/{book}', [BookController::class, 'show'])->name('book.show');

Route::middleware('guest')->group(function () {
    Route::get('login', [LoginController::class, 'create'])->name('login.index');
    Route::post('login', [LoginController::class, 'store'])->name('login.authenticate');
    Route::get('register', [RegisterController::class, 'create'])->name('register.index');
    Route::post('register', [RegisterController::class, 'store'])->name('register.store');
});

Route::middleware('auth')->group(function () {
    Route::post('logout', [LoginController::class, 'destroy'])->name('logout');
    Route::middleware('admin')->prefix('admin')->name('admin.')->group(function() {
        Route::get('', [AdminController::class, 'dashboard'])->name('dashboard');
        Route::prefix('member')->name('member.')->group(function() {
            Route::get('', [AdminController::class, 'dataMember'])->name('index');
            Route::get('pending', [AdminController::class, 'pendingMember'])->name('pending');
            Route::put('verify/{id}', [AdminController::class, 'verifyMember'])->name('verify');
            Route::put('reject/{id}', [AdminController::class, 'rejectMember'])->name('reject');
            Route::get('{member}/edit', [AdminController::class, 'editMember'])->name('edit');
            Route::put('{member}', [AdminController::class, 'updateMember'])->name('update');
            Route::delete('{member}', [AdminController::class, 'deleteMember'])->name('delete');
        });
        Route::prefix('book')->name('book.')->group(function() {
            Route::get('create', [BookController::class, 'create'])->name('create');
            Route::post('', [BookController::class, 'store'])->name('store');
            Route::get('{book}/edit', [BookController::class, 'edit'])->name('edit');
            Route::post('{book}/restore', [BookController::class, 'restore'])->name('restore');
            Route::put('{book}', [BookController::class, 'update'])->name('update');
            Route::delete('{book}', [BookController::class, 'destroy'])->name('destroy');
        });
        Route::prefix('bookborrowing')->name('bookborrowing.')->group(function() {
            Route::get('request', [AdminController::class, 'bookBorrowingRequest'])->name('request');
            Route::get('histories', [AdminController::class, 'bookBorrowingHistories'])->name('histories');
            Route::get('pickup', [AdminController::class, 'bookBorrowingPickup'])->name('pickup');
            Route::get('drop', [AdminController::class, 'bookBorrowingDrop'])->name('drop');
            Route::post('', [AdminController::class, 'bookBorrowingProcess'])->name('process');
            Route::post('{book}/active', [AdminController::class, 'bookBorrowingActive'])->name('activated');
            Route::post('{book}/cancel', [AdminController::class, 'bookBorrowingCancel'])->name('cancel');
            Route::post('{book}/return', [AdminController::class, 'bookBorrowingReturn'])->name('return');
        });
    });
    Route::prefix('member')->name('member.')->group(function() {
        Route::get('', [ProfileController::class, 'index'])->name('index');
        Route::get('edit', [ProfileController::class, 'edit'])->name('edit');
        Route::get('create', [ProfileController::class, 'create'])->name('create');
        Route::post('', [ProfileController::class, 'store'])->name('store');
        Route::put('', [ProfileController::class, 'update'])->name('update');
        Route::get('cart', [BookBorrowingCartController::class, 'index'])->name('cart');
        Route::get('borrowingstatus', [BookBorrowingRequestController::class, 'status'])->name('borrowingstatus');
        Route::get('borrowinghistories', [BookBorrowingRequestController::class, 'histories'])->name('borrowinghistories');
    });
    Route::prefix('cart')->name('cart.')->group(function() {
        Route::post('', [BookBorrowingCartController::class, 'store'])->name('store');
        Route::post('add', [BookBorrowingCartController::class, 'add'])->name('add');
        Route::post('subtract', [BookBorrowingCartController::class, 'subtract'])->name('subtract');
        Route::post('request', [BookBorrowingRequestController::class, 'store'])->name('request');
    });
});